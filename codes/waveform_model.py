"""
File to store the lensing beyond GR waveform with birefringence model.
"""

import numpy as np
from bilby.gw.source import *

def ModelGenericBeyondGRLensingBBH(frequency_array, mass_1, mass_2, \
                                  luminosity_distance, a_1, tilt_1, \
                                  phi_12, a_2, tilt_2, phi_jl, theta_jn,\
                                  phase, t12=0.,lens_angle = 0, **kwargs):
    
    waveform_polarizations = lal_binary_black_hole(frequency_array, mass_1, mass_2, \
                                                                    luminosity_distance, a_1, tilt_1, \
                                                                    phi_12, a_2, tilt_2, phi_jl, theta_jn, \
                                                                    phase, **kwargs)
    Delta=np.exp(1j*frequency_array*2*np.pi*t12)
    hp, hc =waveform_polarizations['plus'], waveform_polarizations['cross']
    
    waveform_polarizations['plus'] *= 1/2.*((Delta-1.)*np.cos(4*lens_angle)+Delta+1)
    waveform_polarizations['plus'] += 1/2.*(Delta-1.)*np.sin(4.*lens_angle)*hc
    waveform_polarizations['cross'] *= 1/2.*(-(Delta-1.)*np.cos(4*lens_angle)+Delta+1)
    waveform_polarizations['cross'] += 1/2.*(Delta-1.)*np.sin(4.*lens_angle)*hp
        
    return waveform_polarizations