import bilby
import matplotlib.pylab as plt
import numpy as np
import pandas as pd
import json
import seaborn as sns
import argparse

parser = argparse.ArgumentParser(
        description='This is stand alone code for plotting corner and 2D posterior for t12 and lens angle for lensing beyond GR test.')
parser.add_argument(
        '-json_file',
        '--json_file',
        help='input JSON file')
parser.add_argument(
        '-outfile',
        '--outfile',
        help='output filename OUTFILE.png ')
args = parser.parse_args()
    
res = bilby.result.read_in_result(args.json_file)

pars =['chirp_mass','mass_ratio','a_1','a_2','theta_jn','psi','phase','t12','lens_angle','geocent_time','ra','dec']
res.plot_corner(parameters=pars, save=True,filename = args.outfile+'_corner.png')

par1='t12'
par2='lens_angle'
plt.figure()
#sns.jointplot(
#    data=res.posterior,
#    x=par1, y=par2,
#    kind="kde")
#plt.savefig(args.outfile + '_2d.png')
# plt.scatter(res[par1],res[par2],c=res['log_likelihood'])
   # plt.colorbar()
    #plt.xlabel(par1)
   # plt.ylabel(par2)