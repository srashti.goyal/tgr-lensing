from pycondor import Dagman, Job
import os

submit_dag=0
t12_inj_arr = [0, 0.001, 0.003, 0.01, 0.03]
phi_lens_inj_arr = [0.6]
snr_inj_arr = [20,40]#[10,15,30]#[20,40]
seed_arr = [888888]
odir = 'GW190814_like_bfs_21082022'

error = os.path.abspath('outdir/condor/error')
output = os.path.abspath('outdir/condor/output')
log = os.path.abspath('outdir/condor/log')
submit = os.path.abspath('outdir/condor/submit')

dagman = Dagman(name='data_gen_dagman',
                    submit=submit)
i=0
for t12 in t12_inj_arr : 
    for phi_lens in phi_lens_inj_arr:
        for snr in snr_inj_arr:
            for seed in seed_arr:
                
                args_gw150914_ngr_rec='-t12_inj %f -phi_lens_inj %f -seed %d -snr_inj %d -odir %s -label %s -gaussian_noise 1 -nGR_lens_rec 1 -nact 20 -npool 16 -npoints 1000'%(t12, phi_lens, seed, snr, odir,'nGR_rec_t12_'+str(t12)+'_phi_' +str(phi_lens))

                args_gw150914_gr_rec='-t12_inj %f -phi_lens_inj %f -seed %d -snr_inj %d -odir %s -label %s -gaussian_noise 1 -nGR_lens_rec 0 -nact 20 -npool 16 -npoints 1000'%(t12, phi_lens, seed, snr, odir,'GR_rec_t12_'+str(t12)+'_phi_' +str(phi_lens))
                
                print(args_gw150914_ngr_rec)
                print(args_gw150914_gr_rec)


                ## non-GR rec
                job = Job(name='args_gw150914_ngr_rec_' + str(i) ,executable='argparse_pe_run_include_phi_spins_HM.py',submit=submit,error=error,\
                          output=output, log=log,arguments=args_gw150914_ngr_rec,request_cpus=8,verbose=1, getenv=True, request_memory='4GB')
                
                dagman.add_job(job)


                ## GR-rec
                job = Job(name='args_gw150914_gr_rec_' + str(i) ,executable='argparse_pe_run_include_phi_spins_HM.py',submit=submit,error=error,\
                          output=output, log=log,arguments=args_gw150914_gr_rec,request_cpus=8,verbose=1, getenv=True, request_memory='4GB')


                dagman.add_job(job)
                i+=1

if submit_dag ==1:       
    'Dag created submitting the jobs...'
    dagman.build_submit()
        
else:
    dagman.build()
    print('\n \n Dag saved at: %s'%(submit +'/' + dagman.name))
