from pycondor import Job
import os

error = os.path.abspath('outdir/condor/error')
output = os.path.abspath('outdir/condor/output')
log = os.path.abspath('outdir/condor/log')
submit = os.path.abspath('outdir/condor/submit')

args_ex='-t12_inj 0 -phi_lens_inj 0 -seed 888881 -snr_inj 20 -odir test_argparse -label gauss_noise_GW150914_like_nGR_rec -gaussian_noise True -nGR_lens_rec True -nact 50 -npool 16 -npoints 1500'

args_gw150914_snr20_convergence='-t12_inj 0 -phi_lens_inj 0 -seed 888885 -snr_inj 20 -odir all_fixed_GW150914_like -label gauss_noise_GW150914_like_GR_inj_nGR_rec -gaussian_noise True -nGR_lens_rec True -nact 50 -npool 16 -npoints 1500'

args_gw150914_snr20='-t12_inj 0 -phi_lens_inj 0 -seed 888885 -snr_inj 20 -odir all_fixed_GW150914_like -label gauss_noise_GW150914_like_GR_inj_nGR_rec_normal -gaussian_noise True -nGR_lens_rec True -nact 20 -npool 16 -npoints 1000'

args_gw150914_snr40='-t12_inj 0 -phi_lens_inj 0 -seed 888885 -snr_inj 40 -odir all_fixed_GW150914_like -label gauss_noise_GW150914_like_GR_inj_nGR_rec_normal -gaussian_noise True -nGR_lens_rec True -nact 20 -npool 16 -npoints 1000'


args_gw150914_snr40_zero_noise='-t12_inj 0 -phi_lens_inj 0 -seed 888884 -snr_inj 40 -odir all_fixed_GW150914_like -label zero_noise_GW150914_like_GR_inj_nGR_rec_normal -gaussian_noise False -nGR_lens_rec True -nact 20 -npool 16 -npoints 1000'

args_gw150914_snr20_zero_noise_convergence='-t12_inj 0 -phi_lens_inj 0 -seed 888884 -snr_inj 20 -odir all_fixed_GW150914_like -label zero_noise_GW150914_like_GR_inj_nGR_rec_normal -gaussian_noise False -nGR_lens_rec True -nact 50 -npool 16 -npoints 1500'

args_gw150914_snr40_ngr_inj='-t12_inj 0.01 -phi_lens_inj 0.627 -seed 888885 -snr_inj 40 -odir all_fixed_GW150914_like -label zero_noise_GW150914_like_nGR_inj_nGR_rec_normal -gaussian_noise False -nGR_lens_rec True -nact 20 -npool 16 -npoints 1000'

args_gw150914_snr20_ngr_inj='-t12_inj 0.01 -phi_lens_inj 0.627 -seed 888885 -snr_inj 20 -odir all_fixed_GW150914_like -label zero_noise_GW150914_like_nGR_inj_nGR_rec_normal -gaussian_noise False -nGR_lens_rec True -nact 20 -npool 16 -npoints 1000'


job = Job(name='args_gw150914_snr40_ngr_inj' ,executable='argparse_pe_run_include_phi_spins_HM.py',submit=submit,error=error,\
          output=output, log=log,arguments=args_gw150914_snr40_ngr_inj,request_cpus=8,verbose=1)
job.build_submit()