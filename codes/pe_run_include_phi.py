#!/home1/srashti.goyal/anaconda3/envs/bilby_env/bin/python
import bilby
import numpy as np
import matplotlib.pylab as plt
import json
import os

ra = 1.7
dec = 1.7
pol = 0.2
inc = 0.45*np.pi#0.45*np.pi#np.pi/18 #np.pi/2
time = 1000000000
t12=-0.03
phi=np.pi/5.
injection_parameters = {'mass_1': 30.0, 'mass_2': 29.0, 'luminosity_distance': 4.5*400., 'psi': pol, 'theta_jn': inc, \
                        'phase': 0, 'geocent_time': time, 'ra': ra, 'dec': dec,'t12': t12,'lens_angle': phi}
         
duration = 8.
sampling_frequency = 2048.

# Specify the output directory and the name of the simulation.
outdir = 'outdir/snr_real'
label = 't12_003_hc_small_phi_pi_by5'
bilby.core.utils.setup_logger(outdir=outdir, label=label)

# Fixed arguments passed into the source model
waveform_arguments = dict(waveform_approximant='IMRPhenomPv2',
                          reference_frequency=20., minimum_frequency=20.)

def pol_model(frequency_array,mass_1, mass_2, luminosity_distance,theta_jn,phase,t12=0.,lens_angle = 0):
    
    waveform_polarizations = bilby.gw.source.lal_binary_black_hole(frequency_array,mass_1=mass_1,mass_2=mass_2,luminosity_distance=luminosity_distance, theta_jn=theta_jn, phase=phase,a_1=0, a_2=0, tilt_1=0, tilt_2=0,\
                                                                   phi_12=0,phi_jl=0,waveform_arguments =waveform_arguments)
    Delta=np.exp(2j*frequency_array*2*np.pi*t12)
    hp, hc =waveform_polarizations['plus'], waveform_polarizations['cross']
    
    waveform_polarizations['plus'] *= 1/2.*((Delta-1.)*np.cos(4*lens_angle)+Delta+1)
    waveform_polarizations['plus'] += 1/2.*(Delta-1.)*np.sin(4.*lens_angle)*hc
    waveform_polarizations['cross'] *= 1/2.*(-(Delta-1.)*np.cos(4*lens_angle)+Delta+1)
    waveform_polarizations['cross'] += 1/2.*(Delta-1.)*np.sin(4.*lens_angle)*hp
        
    return waveform_polarizations

waveform_generator = bilby.gw.WaveformGenerator(
    duration=duration, sampling_frequency=sampling_frequency,
    frequency_domain_source_model=pol_model)



ifos1 = bilby.gw.detector.InterferometerList(['H1', 'L1','V1'])
#ifos1.set_strain_data_from_zero_noise(sampling_frequency=sampling_frequency, duration=duration,start_time=injection_parameters['geocent_time'] - 6)

ifos1.set_strain_data_from_power_spectral_densities(sampling_frequency=sampling_frequency, duration=duration,start_time=injection_parameters['geocent_time'] - 6)


ifos1.inject_signal(waveform_generator=waveform_generator,parameters=injection_parameters)

priors = bilby.gw.prior.BBHPriorDict()

priors['geocent_time'] = bilby.core.prior.Uniform(
    minimum=injection_parameters['geocent_time'] - 1,
    maximum=injection_parameters['geocent_time'] + 1,
    name='geocent_time', latex_label='$t_c$', unit='$s$')
priors['mass_1'] = bilby.core.prior.Uniform(10,50,name='mass_1', latex_label='$m_1$', unit='$Msol$')
priors['mass_2'] = bilby.core.prior.Uniform(10,50,name='mass_2', latex_label='$m_1$', unit='$Msol$')
for key in ['a_1', 'a_2', 'tilt_1', 'tilt_2', 'phi_12', 'phi_jl']:
    priors[key] = 0
priors['t12']=bilby.core.prior.Uniform(-1,1,name='Delta t_12', latex_label='$\Delta t_{12}$', unit='$Msol$')
priors['lens_angle']=bilby.core.prior.Uniform(0,np.pi,name='lens angle', latex_label='$\Phi_{lens}$', unit='$rad$')

likelihood = bilby.gw.GravitationalWaveTransient(
    interferometers=ifos1, waveform_generator=waveform_generator)

# Run sampler.  In this case we're going to use the `dynesty` sampler
result = bilby.run_sampler(
    likelihood=likelihood, priors=priors, sampler='dynesty',
    injection_parameters=injection_parameters, outdir=outdir, label=label,npoints=1000,nact=20,npool=2) 
### nact change to 20, nlive 1000, chains in dynesty?

# Make a corner plot.
result.plot_corner()