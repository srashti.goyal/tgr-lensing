from pycondor import Dagman, Job
import os
import time

submit_dag=0
#odir = '/home1/srashti.goyal/public_html/tgr-lensing/26062022_GR'
odir = '/home1/srashti.goyal/public_html/tgr-lensing/14062022'


error = os.path.abspath('../outdir/condor/error')
output = os.path.abspath('../outdir/condor/output')
log = os.path.abspath('../outdir/condor/log')
submit = os.path.abspath('../outdir/condor/submit')

dagman = Dagman(name='real_event_tgr_lensing',
                    submit=submit)
#events=['S190728q']
events = [ 'S190408an','S190412m', 'S190421ar', 'S190503bf', 'S190512at', 'S190513bm', 'S190517h', 'S190519bj', 'S190521g', 'S190521r', 'S190602aq', 'S190630ag', 'S190706ai', 'S190707q', 'S190708ap', 'S190720a', 'S190727h', 'S190728q', 'S190814bv', 'S190828j', 'S190828l', 'S190910s', 'S190915ak', 'S190924h', 'S191109d', 'S191129u', 'S191204r', 'S191215w', 'S191216ap', 'S191222n', 'S200112r', 'S200129m', 'S200202ac', 'S200208q', 'S200219ac', 'S200224ca', 'S200225q', 'S200311bg', 'S200316bj']


running = [898]#[0,2,6]
for i,event in enumerate(events) : 
    if i not in running:
        if  os.path.exists(odir+'/'+event+'/'+event+'_result.json'):#'_resume.pickle'):
          #  print(odir+'/'+event+'/'+event+'_resume.pickle', 'resume file exists already') 
            print(odir+'/'+event+'/'+event+'_result.json', 'result file exists already')
        else:
            args_ngr_rec='-event_id %s -outdir %s -label %s -nGR_lens_rec 1'%(event,odir+'/'+event,event)

            print(args_ngr_rec)

            ## non-GR rec
            job = Job(name=event ,executable='Image1runs.py',submit=submit,error=error,\
                  output=output, log=log,arguments=args_ngr_rec,request_cpus=8,verbose=1, getenv=True, universe='vanilla',request_memory='8GB')#,retry=4)
            job.build_submit()
            time.sleep(60*1)
        
            dagman.add_job(job)

if submit_dag ==1:       
    'Dag created submitting the jobs...'
    dagman.build_submit()
        
else:
    dagman.build()
    print('\n \n Dag saved at: %s'%(submit +'/' + dagman.name))
