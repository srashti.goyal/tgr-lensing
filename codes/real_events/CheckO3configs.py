import yaml 
import json

O3a_file = 'O3aledger.yaml'
O3b_file = 'O3bledger.yaml'
config_file = 'EventConfigs.json'
outfile = 'ComarisonGOLUM_Ledger.txt'




with open(O3a_file) as O3af:
    data_O3a = yaml.safe_load(O3af)

with open(O3b_file) as O3bf:
    data_O3b = yaml.safe_load(O3bf)

with open(config_file) as config:
    EvConf = json.load(config)


events = list(EvConf.keys()).copy()
verified_events = dict(o3a = [], o3b = [])
o3a_ledger_events = dict()
o3b_ledger_events = dict()


print(" ")
print("#############################################")
print(" ")
print("Analyzing data for O3a ledger")
print(" ")
print("#############################################")
print(" ")

for j in range(len(data_O3a)):
    print("\n ")
    print("++++    Looking for event %s    ++++\n" %(data_O3a[j]['old superevent']))
    print("\n")
    ev = data_O3a[j]['old superevent']
    if ev not in events:
        print(" ")
        print("!!! %s Is not considered in the lensing analyses !!!\n" %ev)
    else:
        o3a_ledger_events[ev] = dict()
        for i in range(len(data_O3a[j]['productions'])):
            for key in data_O3a[j]['productions'][i].keys():
                if len(data_O3a[j]['productions'][i][key]['review']) > 0:
                    for ii in range(len(data_O3a[j]['productions'][i][key]['review'])):
                        if data_O3a[j]['productions'][i][key]['review'][ii]['status'] == 'APPROVED': 
                            o3a_ledger_events[ev]['prod'] = key
                            o3a_ledger_events[ev]['full name'] = data_O3a[j]['full name']

                            
                            if 'approximant' in data_O3a[j]['productions'][i][key].keys():
                                wf = data_O3a[j]['productions'][i][key]['approximant']
                                o3a_ledger_events[ev][wf] = dict()
                                #print(key, "  ", data_O3a[j]['productions'][i][key]['review'][ii]['status'], " WF:  ", data_O3a[j]['productions'][i][key]['approximant'], " \n")
                            else:
                                wf = 'No_WF'
                                o3a_ledger_events[ev][wf] = dict()
                                #print(key, "  ", data_O3a[j]['productions'][i][key]['review'][ii]['status'], " \n")
                            #print(" \n ")
                            #print("IFOS ledger:  ", data_Oab[j]['interferometers'])
                            o3a_ledger_events[ev][wf]['ifos'] = data_O3a[j]['interferometers']
                            #print("IFOS GOLUM:  ", EvConf[ev]['ifos_list'], " \n")
                            #print(" \n")
                            #print("Channels Ledger:   ", data_O3a[j]['data']['channels'], " \n")
                            o3a_ledger_events[ev][wf]['channels'] = data_O3a[j]['data']['channels']
                            #print("Channels GOLUM:  ", EvConf[ev]['channels'], " \n")
                            #print(" \n")
                            #print("PSDs Ledger:  ", data_O3a[j]['productions'][i][key]['psds'], " \n")
                            o3a_ledger_events[ev][wf]['psd_files'] = data_O3a[j]['productions'][i][key]['psds']
                            #print("PSDS GOLUM:  ", EvConf[ev]['psd_files'], " \n")
                            #print(" \n")
                            #print("Calibration files Ledger:   ", data_O3a[j]['calibration'], " \n")
                            o3a_ledger_events[ev][wf]['calibration_files'] = data_O3a[j]['calibration']
                            #print("Calibration files GOLUM:  ", EvConf[ev]['calibration_files'], " \n")
                            #print(" \n")
                            #print("Trig time Ledger:  ", data_O3a[j]['event time'], " \n")
                            o3a_ledger_events[ev][wf]['trigger_time'] = data_O3a[j]['event time']
                            #print("Trig time GOLUM:  ", EvConf[ev]['trigger_time'], " \n")
                            #print(" \n")
                            #print("Minimum freqs Ledger:  ", data_O3a[j]['quality']['lower-frequency'], " \n")
                            o3a_ledger_events[ev][wf]['minimum_frequency'] = data_O3a[j]['quality']['lower-frequency']
                            #print("Minimum freqs GOLUM: ", EvConf[ev]['minimum_frequency'], " \n")
                            #print(" \n")
                            if 'high-frequency' in data_O3a[j]['quality'].keys():
                                o3a_ledger_events[ev][wf]['maximum_frequency'] = data_O3a[j]['quality']['high-frequency']
                                #print("Maximum freqs Ledger:  ", data_O3a[j]['quality']['high-frequency'], " \n")
                            elif 'upper-frequency' in data_O3a[j]['quality'].keys():
                                #print("Maximum freqs Ledger:  ",data_O3a[j]['quality']['upper-frequency'] , " \n")
                                o3a_ledger_events[ev][wf]['maximum_frequency'] = data_O3a[j]['quality']['upper-frequency']
                            else:
                                #print("Maximum freqs ledger:  ", float(data_O3a[j]['quality']['sample-rate'])/2., " \n")
                                o3a_ledger_events[ev][wf]['maximum_frequency'] = float(data_O3a[j]['quality']['sample-rate'])/2.
                            #print("Maximum freqs GOLUM:  ", EvConf[ev]['maximum_frequency'], " \n")
                            #print(" \n")
                            #print("Duration ledger:  ", data_O3a[j]['quality']['segment-length'], " \n")
                            o3a_ledger_events[ev][wf]['duration'] = data_O3a[j]['quality']['segment-length']
                            #print("Duration GOLUM:  ", EvConf[ev]['duration'], " \n")
                            #print(" \n")
                            #print("PSD Duration ledger:  ", data_O3a[j]['quality']['psd-length'], " \n")
                            o3a_ledger_events[ev][wf]['psd_duration'] = data_O3a[j]['quality']['psd-length']
                            #print("PSD duration GOLUM:  ", EvConf[ev]['psd_duration'], " \n")
                            #print(" \n")
                            #print("Sampling freq ledger:  ", data_O3a[j]['quality']['sample-rate'], " \n")
                            o3a_ledger_events[ev][wf]['sampling_frequency'] = data_O3a[j]['quality']['sample-rate']
                            #print("Sampling freq GOLUM:  ", EvConf[ev]['sampling_frequency'], " \n")
                            #print(" \n")
                            #print("++++++++++++++++++++++++++++++++++++++++\n")
                            verified_events['o3a'].append(ev)
                        else:
                            print("!!! The status of the run in not approved, it is:  ", data_O3a[j]['productions'][i][key]['review'][ii]['status'], " \n")
                else:
                    print(" ")
                    print("!!! No reviewed run is available, the data has probably been taken elsewhere !!!\n")

print(" ")
print("#############################################")
print(" ")
print("Analyzing data for O3b ledger")
print(" ")
print("#############################################")
print(" ")

for j in range(len(data_O3b)):
    print("\n ")
    print("++++    Looking for event %s    ++++\n" %(data_O3b[j]['old superevent']))
    print("\n")
    ev = data_O3b[j]['old superevent']
    if ev not in events:
        print(" ")
        print("!!! %s Is not considered in the lensing analyses !!!\n" %ev)
    else:
        o3b_ledger_events[ev] = dict()
        for i in range(len(data_O3b[j]['productions'])):
            for key in data_O3b[j]['productions'][i].keys():
                if len(data_O3b[j]['productions'][i][key]['review']) > 0:
                    for ii in range(len(data_O3b[j]['productions'][i][key]['review'])):
                        if data_O3b[j]['productions'][i][key]['review'][ii]['status'] == 'APPROVED':
                            o3b_ledger_events[ev]['prod'] = key
                            o3b_ledger_events[ev]['full name'] = data_O3b[j]['full name']
                            #print(key, data_O3b[j]['productions'][i][key])
                            if 'approximant' in data_O3b[j]['productions'][i][key].keys():
                                wf = data_O3b[j]['productions'][i][key]['approximant']
                                o3b_ledger_events[ev][wf] = dict()
                                #print(key, "  ", data_O3b[j]['productions'][i][key]['review'][ii]['status'], " WF:  ", data_O3b[j]['productions'][i][key]['approximant'], " \n")
                            else:
                                wf = 'No_WF'
                                o3b_ledger_events[ev][wf] = dict()
                                #print(key, "  ", data_O3b[j]['productions'][i][key]['review'][ii]['status'], " \n")
                            #print(" \n ")
                            #print("IFOS ledger:  ", data_Oab[j]['interferometers'])
                            o3b_ledger_events[ev][wf]['ifos'] = data_O3b[j]['interferometers']
                            #print("IFOS GOLUM:  ", EvConf[ev]['ifos_list'], " \n")
                            #print(" \n")
                            #print("Channels Ledger:   ", data_o3b[j]['data']['channels'], " \n")
                            o3b_ledger_events[ev][wf]['channels'] = data_O3b[j]['data']['channels']
                            #print("Channels GOLUM:  ", EvConf[ev]['channels'], " \n")
                            #print(" \n")
                            #print("PSDs Ledger:  ", data_O3b[j]['productions'][i][key]['psds'], " \n")
                            o3b_ledger_events[ev][wf]['psd_files'] = data_O3b[j]['productions'][i][key]['psds']
                            #print("PSDS GOLUM:  ", EvConf[ev]['psd_files'], " \n")
                            #print(" \n")
                            #print("Calibration files Ledger:   ", data_O3b[j]['calibration'], " \n")
                            o3b_ledger_events[ev][wf]['calibration_files'] = data_O3b[j]['calibration']
                            #print("Calibration files GOLUM:  ", EvConf[ev]['calibration_files'], " \n")
                            #print(" \n")
                            #print("Trig time Ledger:  ", data_O3b[j]['event time'], " \n")
                            o3b_ledger_events[ev][wf]['trigger_time'] = data_O3b[j]['event time']
                            #print("Trig time GOLUM:  ", EvConf[ev]['trigger_time'], " \n")
                            #print(" \n")
                            #print("Minimum freqs Ledger:  ", data_O3b[j]['quality']['lower-frequency'], " \n")
                            o3b_ledger_events[ev][wf]['minimum_frequency'] = data_O3b[j]['quality']['lower-frequency']
                            #print("Minimum freqs GOLUM: ", EvConf[ev]['minimum_frequency'], " \n")
                            #print(" \n")
                            if 'high-frequency' in data_O3b[j]['quality'].keys():
                                o3b_ledger_events[ev][wf]['maximum_frequency'] = data_O3b[j]['quality']['high-frequency']
                                #print("Maximum freqs Ledger:  ", data_O3b[j]['quality']['high-frequency'], " \n")
                            elif 'upper-frequency' in data_O3b[j]['quality'].keys():
                                #print("Maximum freqs Ledger:  ",data_O3b[j]['quality']['upper-frequency'] , " \n")
                                o3b_ledger_events[ev][wf]['maximum_frequency'] = data_O3b[j]['quality']['upper-frequency']
                            else:
                                #print("Maximum freqs ledger:  ", float(data_O3b[j]['quality']['sample-rate'])/2., " \n")
                                o3b_ledger_events[ev][wf]['maximum_frequency'] = float(data_O3b[j]['quality']['sample-rate'])/2.
                            #print("Maximum freqs GOLUM:  ", EvConf[ev]['maximum_frequency'], " \n")
                            #print(" \n")
                            #print("Duration ledger:  ", data_O3b[j]['quality']['segment-length'], " \n")
                            o3b_ledger_events[ev][wf]['duration'] = data_O3b[j]['quality']['segment-length']
                            #print("Duration GOLUM:  ", EvConf[ev]['duration'], " \n")
                            #print(" \n")
                            #print("PSD Duration ledger:  ", data_O3b[j]['quality']['psd-length'], " \n")
                            o3b_ledger_events[ev][wf]['psd_duration'] = data_O3b[j]['quality']['psd-length']
                            #print("PSD duration GOLUM:  ", EvConf[ev]['psd_duration'], " \n")
                            #print(" \n")
                            #print("Sampling freq ledger:  ", data_O3b[j]['quality']['sample-rate'], " \n")
                            o3b_ledger_events[ev][wf]['sampling_frequency'] = data_O3b[j]['quality']['sample-rate']
                            #print("Sampling freq GOLUM:  ", EvConf[ev]['sampling_frequency'], " \n")
                            #print(" \n")
                            #print("++++++++++++++++++++++++++++++++++++++++\n")
                            verified_events['o3b'].append(ev)
                        else:
                            print("!!! The status of the run in not approved, it is:  ", data_O3b[j]['productions'][i][key]['review'][ii]['status'], " \n")
                else:
                    print(" ")
                    print("!!! No reviewed run is available, the data has probably been taken elsewhere !!!\n")

print("\n ")
print("######################################")
print(" ")
print("Ledger analysis is done")
print(" ")
print("######################################")
print(" ")

# printing out only one element 
print("  ")
print("################################################")
print(" ")
print("OUTPUT of the analysis")
print(" ")
print("################################################")
print(" ")
print("++++  For O3a  ++++")
print(" ")

for ev in o3a_ledger_events.keys():
    
    if len(o3a_ledger_events[ev].keys()) > 0:
        print(" ")
        print("  ++  Event:  %s , Fullname %s ++"%(ev,o3a_ledger_events[ev]['full name']))
        print(" ")
        print("  ++  Prod:  %s  ++"%o3a_ledger_events[ev]['prod'])

        if 'IMRPhenomXPHM' in o3a_ledger_events[ev].keys():
            wf = 'IMRPhenomXPHM'
        elif 'No_WF' in o3a_ledger_events[ev].keys():
            wf = 'No_WF'
        else:
            wf = 'SEOBNRv4PHM'

        print("IFOS ledger:  ", o3a_ledger_events[ev][wf]['ifos'])
        print("IFOS golum:  ", EvConf[ev]['ifos_list'])
        print(" ")
        print("channels ledger:  ", o3a_ledger_events[ev][wf]['channels'])
        print("channels golum:  ", EvConf[ev]['channels'])
        print(" ")
        print("PSDs ledger:  ", o3a_ledger_events[ev][wf]['psd_files'])
        print("PSDs golum: ", EvConf[ev]['psd_files'])
        print(" ")
        print("Calibration ledger:  ", o3a_ledger_events[ev][wf]['calibration_files'])
        print("Calibration golum:  ", EvConf[ev]['calibration_files'])
        print(" ")
        print("Trigger time ledger:  ", o3a_ledger_events[ev][wf]['trigger_time'])
        print("Trigger time golum:  ", EvConf[ev]['trigger_time'])
        print(" ")
        print("Minimum freq ledger:  ", o3a_ledger_events[ev][wf]['minimum_frequency'])
        print("Minimum freq golum:  ", EvConf[ev]['minimum_frequency'])
        print(" ")
        print("Maximum freq ledger:  ", o3a_ledger_events[ev][wf]['maximum_frequency'])
        print("Maximum freq golum:  ", EvConf[ev]['maximum_frequency'])
        print(" ")
        print("Duration ledger:  ", o3a_ledger_events[ev][wf]['duration'])
        print("Duration golum:  ", EvConf[ev]['duration'])
        print(" ")
        print("PSD duration ledger:  ", o3a_ledger_events[ev][wf]['psd_duration'])
        print("PSD duration golum:  ", EvConf[ev]['psd_duration'])
        print(" ")
        print("Sampling freq ledger:  ", o3a_ledger_events[ev][wf]['sampling_frequency'])
        print("Sampling freq GOLUM:  ", EvConf[ev]['sampling_frequency'])
        print(" ")

print(" ")
print("++++  For O3b  ++++")
print(" ")
for ev in o3b_ledger_events.keys():
    if len(o3b_ledger_events[ev].keys()) > 0:
        print(" ")
        print("  ++  Event:  %s , Fullname %s ++"%(ev,o3b_ledger_events[ev]['full name']))

        print("  ++  Prod:  %s  ++"%o3b_ledger_events[ev]['prod'])
        print(" ")
        if 'IMRPhenomXPHM' in o3b_ledger_events[ev].keys():
            wf = 'IMRPhenomXPHM'
        elif 'No_WF' in o3b_ledger_events[ev].keys():
            wf = 'No_WF'
        else:
            wf = 'SEOBNRv4PHM'

        print("IFOS ledger:  ", o3b_ledger_events[ev][wf]['ifos'])
        print("IFOS golum:  ", EvConf[ev]['ifos_list'])
        print(" ")
        print("channels ledger:  ", o3b_ledger_events[ev][wf]['channels'])
        print("channels golum:  ", EvConf[ev]['channels'])
        print(" ")
        print("PSDs ledger:  ", o3b_ledger_events[ev][wf]['psd_files'])
        print("PSDs golum: ", EvConf[ev]['psd_files'])
        print(" ")
        print("Calibration ledger:  ", o3b_ledger_events[ev][wf]['calibration_files'])
        print("Calibration golum:  ", EvConf[ev]['calibration_files'])
        print(" ")
        print("Trigger time ledger:  ", o3b_ledger_events[ev][wf]['trigger_time'])
        print("Trigger time golum:  ", EvConf[ev]['trigger_time'])
        print(" ")
        print("Minimum freq ledger:  ", o3b_ledger_events[ev][wf]['minimum_frequency'])
        print("Minimum freq golum:  ", EvConf[ev]['minimum_frequency'])
        print(" ")
        print("Maximum freq ledger:  ", o3b_ledger_events[ev][wf]['maximum_frequency'])
        print("Maximum freq golum:  ", EvConf[ev]['maximum_frequency'])
        print(" ")
        print("Duration ledger:  ", o3b_ledger_events[ev][wf]['duration'])
        print("Duration golum:  ", EvConf[ev]['duration'])
        print(" ")
        print("PSD duration ledger:  ", o3b_ledger_events[ev][wf]['psd_duration'])
        print("PSD duration golum:  ", EvConf[ev]['psd_duration'])
        print(" ")
        print("Sampling freq ledger:  ", o3b_ledger_events[ev][wf]['sampling_frequency'])
        print("Sampling freq GOLUM:  ", EvConf[ev]['sampling_frequency'])
        print(" ")

# verifying which event have there info gotten somewhere else
print(" ")
print("The O3a events which are not here are taken from GWTC 2.1")
print(" ")
print("Events not represented here:")
for i in range(len(events)):
    if events[i] not in verified_events['o3a'] and events[i] not in verified_events['o3b']:
        print("%s"%events[i])
print(" ")
print("Done")
