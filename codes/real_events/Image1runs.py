#!/home1/aditya.vijaykumar/src/miniconda3/envs/lensingphase/bin/python
####!/home1/srashti.goyal/anaconda3/envs/bilby_env/bin/python
####!/home/justin.janquart/.conda/envs/igwnGolumRev/bin/python
import bilby 
import numpy as np
import json
import matplotlib.pyplot as plt
from tgrlensing import waveform_model
import argparse
import os
import subprocess
import sys

os.environ['KRB5_KTNAME'] = "/home1/srashti.goyal/.kerberos/ligo.org.keytab"
os.environ['KRB5CCNAME']="/home1/srashti.goyal/krb5_ticket_u1051"

parser = argparse.ArgumentParser(description = 'Core code to run the GOLUM analyses for the first images')
parser.add_argument('-event_id', '--event_id', help = 'Name of the event to be analyzed')
parser.add_argument('-outdir', '--outdir', help = 'Name of the out directory')
parser.add_argument('-label', '--label', help = 'label t be used for the run')
parser.add_argument(
        '-nGR_lens_rec',
        '--nGR_lens_rec',
        type=int,
        help='lensing beyond GR recovery: True, GR recovery: False',
        default= 0)
args = parser.parse_args()

# read the arguments 
event = args.event_id
outdir = args.outdir
label = args.label

# read the json file for the run and load specific info
#fileName = 'EventConfigs.json'
fileName = 'EventConfigs_edited.json'
file = open(fileName)
data = json.load(file)

event_specs = data['%s'%event]
data_dir_psd_cals = '/home1/srashti.goyal/O3'
channel_dict = event_specs['channels'].copy()
psd_files_1 = event_specs['psd_files'].copy()
psd_files={}
for det in psd_files_1.keys():
    psd_file=psd_files_1[det]
    psd_files[det]=data_dir_psd_cals+'/'+event +'/psd_files/'+psd_file.split('/')[-1]
#print(psd_files)

spectral_envelopes_1 = event_specs['calibration_files'].copy()
spectral_envelopes={}
for det in spectral_envelopes_1.keys():
    calibration=spectral_envelopes_1[det]
    spectral_envelopes[det]=data_dir_psd_cals+'/'+event +'/calibration_files/'+calibration.split('/')[-1]
#print(spectral_envelopes)
trigger_time = event_specs['trigger_time']
minimum_frequencies = event_specs['minimum_frequency']
maximum_frequencies = event_specs['maximum_frequency']
duration = event_specs['duration']
psd_duration = event_specs['duration']
sampling_frequency = event_specs['sampling_frequency']
reference_frequency = event_specs['reference_frequency']
m1_min = event_specs['prior_kwargs']['m1_min']
m1_max = event_specs['prior_kwargs']['m1_max']
m2_min = event_specs['prior_kwargs']['m2_min']
m2_max = event_specs['prior_kwargs']['m2_max']
mc_min = event_specs['prior_kwargs']['mc_min']
mc_max = event_specs['prior_kwargs']['mc_max']
q_min = event_specs['prior_kwargs']['q_min']
q_max = event_specs['prior_kwargs']['q_max']
dl_min = event_specs['prior_kwargs']['dl_min']
dl_max = event_specs['prior_kwargs']['dl_max']

# setup the logger and print info for the run 
logger = bilby.core.utils.logger
bilby.core.utils.setup_logger(outdir = outdir, label = label)

gps_start_time = trigger_time + 2 - duration 

# setup the interferometers that should be used
interferometers = bilby.gw.detector.InterferometerList([])

for ifo in channel_dict:
    interferometer = bilby.gw.detector.get_empty_interferometer(ifo)
    interferometer.set_strain_data_from_channel_name(channel_dict[ifo],
                                                     sampling_frequency = sampling_frequency,
                                                     duration = duration, 
                                                     start_time = gps_start_time)
    interferometer.minimum_frequency = minimum_frequencies[ifo]
    interferometer.maximum_frequency = maximum_frequencies[ifo]
    interferometer.power_spectral_density = bilby.gw.detector.PowerSpectralDensity(psd_file = psd_files[interferometer.name])
    interferometers.append(interferometer)


# plot the data for the ifos (most a sanity check)
logger.info("Saving the data plots to {}".format(outdir))
bilby.core.utils.check_directory_exists_and_if_not_mkdir(outdir)
interferometers.plot_data(outdir = outdir, label = label)

# setup the prior for the run
Priors = bilby.gw.prior.BBHPriorDict()
Priors['mass_ratio'] = bilby.core.prior.Uniform(name = 'mass_ratio', minimum = q_min,
                                                maximum = q_max, latex_label = '$q$')
Priors['chirp_mass'] = bilby.core.prior.Uniform(name = 'chirp_mass', minimum = mc_min,
                                                maximum = mc_max, latex_label = '$M_{c}$')
Priors['mass_1'] = bilby.core.prior.Constraint(name = 'mass_1', minimum = m1_min, maximum = m1_max)
Priors['mass_2'] = bilby.core.prior.Constraint(name = 'mass_2', minimum = m2_min, maximum = m2_max)
Priors['phase'] = bilby.core.prior.Uniform(name = 'phase', minimum = 0., maximum = 2*np.pi,
                                            boundary = 'periodic')
Priors['ra'] = bilby.prior.Uniform(name = 'ra', latex_label = '$\alpha$', minimum = 0.,
                                    maximum = 2*np.pi)
Priors['dec'] = bilby.prior.Cosine(name = 'dec', latex_label = '$\delta$', minimum = -np.pi/2.,
                                   maximum = +np.pi/2.)
Priors['luminosity_distance'] = bilby.gw.prior.UniformComovingVolume(name = 'luminosity_distance',
                                                                     latex_label = '$D_l$',
                                                                     minimum = dl_min,
                                                                     maximum = dl_max)
Priors['theta_jn'] = bilby.core.prior.Sine(name = 'theta_jn', latex_label = '$\iota$',
                                           minimum = 0., maximum = np.pi)
Priors['geocent_time'] = bilby.core.prior.Uniform(minimum = trigger_time - 0.1,
                                                  maximum = trigger_time + 0.1,
                                                  name = 'geocent_time', latex_label = '$t_c$', unit = '$s$')
Priors['psi'] = bilby.core.prior.Uniform(name = 'psi', minimum = 0., maximum = np.pi,
                                         boundary = 'periodic')
Priors['a_1'] = bilby.core.prior.Uniform(name = 'a_1', minimum = 0., maximum = 0.99)
Priors['a_2'] = bilby.core.prior.Uniform(name = 'a_2', minimum = 0., maximum = 0.99)
Priors['tilt_1'] = bilby.core.prior.Sine(name = 'tilt_1')
Priors['tilt_2'] = bilby.core.prior.Sine(name = 'tilt_2')
Priors['phi_12'] = bilby.core.prior.Uniform(name = 'phi_12', minimum = 0., maximum = 2*np.pi,
                                            boundary = 'periodic')
Priors['phi_jl'] = bilby.core.prior.Uniform(name = 'phi_jl', minimum = 0., maximum = 2*np.pi,
                                            boundary = 'periodic')

if args.nGR_lens_rec == 1:
    ### nGR recovery
    Priors['t12']=bilby.core.prior.Uniform(-0.1,0.1,name='Delta t_12', latex_label='$\Delta t_{12}$', unit='$Msol$')
    Priors['lens_angle']=bilby.core.prior.Uniform(0,np.pi/2,name='lens angle', latex_label='$\Phi_{lens}$', unit='$rad$')
    
else:

    ### GR recovery
    Priors['t12']=0
    Priors['lens_angle']=0
    
#Priors['n_phase'] = prior.MorseFactorPrior(name = 'n_phase', latex_label = '$n_{1}$')

waveform_arguments = dict(waveform_approximant = 'IMRPhenomXPHM', reference_frequency = reference_frequency)
waveform_generator = bilby.gw.WaveformGenerator(duration = duration, sampling_frequency = sampling_frequency, frequency_domain_source_model = waveform_model.ModelGenericBeyondGRLensingBBH, waveform_arguments = waveform_arguments)

for ifo in interferometers:
    ifo.calibration_model = bilby.gw.calibration.CubicSpline(prefix = 'recalib_{}_'.format(ifo.name),
                                                             minimum_frequency = minimum_frequencies[ifo.name],
                                                             maximum_frequency = maximum_frequencies[ifo.name],
                                                             n_points = 10)
    calib_prior = bilby.gw.prior.CalibrationPriorDict().from_envelope_file(
                                spectral_envelopes[ifo.name], 
                                minimum_frequency = minimum_frequencies[ifo.name],
                                maximum_frequency = maximum_frequencies[ifo.name],
                                n_nodes = 10, label = ifo.name)
    Priors.update(calib_prior)

# setup the likelihood
Likelihood = bilby.gw.likelihood.GravitationalWaveTransient(interferometers = interferometers,
                                                            waveform_generator = waveform_generator,
                                                            priors = Priors)

results = bilby.run_sampler(likelihood = Likelihood, priors = Priors, npoints = 2048.,
                            sampler = 'dynesty', nact = 50, npool = 16,
                            maxmcmc = 10000, outdir = outdir, label = label)
results.plot_corner() 
