from pycondor import Dagman, Job
import os
import time

submit_dag=0
#odir = '/home1/srashti.goyal/public_html/tgr-lensing/26062022_GR'
#odir = '/home1/srashti.goyal/public_html/tgr-lensing/14062022'
odir_GR = '/home1/srashti.goyal/public_html/tgr-lensing/16072022_reruns/GR'
odir_nGR = '/home1/srashti.goyal/public_html/tgr-lensing/16072022_reruns/nGR'




error = os.path.abspath('../outdir/condor/error')
output = os.path.abspath('../outdir/condor/output')
log = os.path.abspath('../outdir/condor/log')
submit = os.path.abspath('../outdir/condor/submit')

dagman = Dagman(name='real_event_tgr_lensing',
                    submit=submit)

events = [ 'S190421ar','S190519bj', 'S190521r', 'S190602aq',  'S190814bv',  'S190915ak', 'S190924h', 'S190521g']

running = [898]#[0,2,6]
for i,event in enumerate(events) : 
    if i not in running:
        if os.path.exists(odir_GR+'/'+event+'/'+event+'_resume.pickle'):
            print(odir_GR+'/'+event+'/'+event+'_resume.pickle', 'resume file exists already')        
        else:
            args_ngr_rec='-event_id %s -outdir %s -label %s -nGR_lens_rec 0'%(event,odir_GR+'/'+event,event)

            print(args_ngr_rec)

            ## GR rec
            job = Job(name=event ,executable='Image1runs.py',submit=submit,error=error,\
                  output=output, log=log,arguments=args_ngr_rec,request_cpus=8,verbose=1, getenv=True, universe='vanilla',request_memory='8GB')#,retry=4)
            job.build_submit()
            time.sleep(60*5)
        
            dagman.add_job(job)
            
        if os.path.exists(odir_nGR+'/'+event+'/'+event+'_resume.pickle'):
            print(odir_nGR+'/'+event+'/'+event+'_resume.pickle', 'resume file exists already')        
        else:
            args_ngr_rec='-event_id %s -outdir %s -label %s -nGR_lens_rec 1'%(event,odir_nGR+'/'+event,event)

            print(args_ngr_rec)

            ## non-GR rec
            job = Job(name=event ,executable='Image1runs.py',submit=submit,error=error,\
                  output=output, log=log,arguments=args_ngr_rec,request_cpus=8,verbose=1, getenv=True, universe='vanilla',request_memory='8GB')#,retry=4)
            job.build_submit()
            time.sleep(60*5)
        
            dagman.add_job(job)
            

if submit_dag ==1:       
    'Dag created submitting the jobs...'
    dagman.build_submit()
        
else:
    dagman.build()
    print('\n \n Dag saved at: %s'%(submit +'/' + dagman.name))
