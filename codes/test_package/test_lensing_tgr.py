import numpy as np
from bilby.gw.source import lal_binary_black_hole
from copy import deepcopy


def ModelGenericBeyondGRLensingBBH(
    frequency_array,
    mass_1,
    mass_2,
    luminosity_distance,
    a_1,
    tilt_1,
    phi_12,
    a_2,
    tilt_2,
    phi_jl,
    theta_jn,
    phase,
    t12=0.0,
    lens_angle=0,
    **kwargs
):

    waveform_polarizations = lal_binary_black_hole(
        frequency_array,
        mass_1,
        mass_2,
        luminosity_distance,
        a_1,
        tilt_1,
        phi_12,
        a_2,
        tilt_2,
        phi_jl,
        theta_jn,
        phase,
        **kwargs
    )
    Delta = np.exp(1j * frequency_array * 2 * np.pi * t12)
    hp, hc = waveform_polarizations["plus"], waveform_polarizations["cross"]
    hp_base, hc_base = deepcopy(hp), deepcopy(hc)

    waveform_polarizations["plus"] *= (
        1 / 2.0 * ((Delta - 1.0) * np.cos(4 * lens_angle) + Delta + 1)
    )
    waveform_polarizations["plus"] += (
        1 / 2.0 * (Delta - 1.0) * np.sin(4.0 * lens_angle) * hc
    )
    waveform_polarizations["cross"] *= (
        1 / 2.0 * (-(Delta - 1.0) * np.cos(4 * lens_angle) + Delta + 1)
    )
    waveform_polarizations["cross"] += (
        1 / 2.0 * (Delta - 1.0) * np.sin(4.0 * lens_angle) * hp
    )

    try:
        np.testing.assert_allclose(hp_base, hp)
    except Exception:
        print("Srashti's source model is bad for hp")

    try:
        np.testing.assert_allclose(hc_base, hc)
    except Exception:
        print("Srashti's source model is bad for hc")

    return waveform_polarizations


def ModelGenericBeyondGRLensingBBHAditya(
    frequency_array,
    mass_1,
    mass_2,
    luminosity_distance,
    a_1,
    tilt_1,
    phi_12,
    a_2,
    tilt_2,
    phi_jl,
    theta_jn,
    phase,
    t12=0.0,
    lens_angle=0,
    **kwargs
):

    waveform_polarizations = lal_binary_black_hole(
        frequency_array,
        mass_1,
        mass_2,
        luminosity_distance,
        a_1,
        tilt_1,
        phi_12,
        a_2,
        tilt_2,
        phi_jl,
        theta_jn,
        phase,
        **kwargs
    )
    Delta = np.exp(1j * frequency_array * 2 * np.pi * t12)
    hp, hc = waveform_polarizations["plus"], waveform_polarizations["cross"]
    hp_base, hc_base = deepcopy(hp), deepcopy(hc)

    edited_polarizations = dict()

    edited_polarizations["plus"] = (
        hp * 1 / 2.0 * ((Delta - 1.0) * np.cos(4 * lens_angle) + Delta + 1)
        + 1 / 2.0 * (Delta - 1.0) * np.sin(4.0 * lens_angle) * hc
    )

    edited_polarizations["cross"] = (
        1 / 2.0 * (-(Delta - 1.0) * np.cos(4 * lens_angle) + Delta + 1) * hc
        + 1 / 2.0 * (Delta - 1.0) * np.sin(4.0 * lens_angle) * hp
    )

    try:
        np.testing.assert_allclose(hp_base, hp)
    except Exception:
        print("Aditya's source model is bad for hp")

    try:
        np.testing.assert_allclose(hc_base, hc)
    except Exception:
        print("Aditya's source model is bad for hc")

    return edited_polarizations
