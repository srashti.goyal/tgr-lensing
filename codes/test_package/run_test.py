import bilby
import test_lensing_tgr
import numpy as np
import matplotlib.pyplot as plt

duration = 4
sampling_frequency = 4096

params = dict(
    mass_1=30.0,
    mass_2=20.0,
    luminosity_distance=1000.0,
    psi=0,
    phase=0,
    geocent_time=1126259462.0,
    ra=0,
    dec=0,
    theta_jn=0.5235987755982987,
    phi_jl=0.0,
    tilt_1=1.5707963267948966,
    tilt_2=1.5707963267948966,
    phi_12=0.0,
    a_1=0.0,
    a_2=0.0,
    lens_angle=np.pi / 3,
    t12=0.1,
)


waveform_arguments = dict(
    waveform_approximant="IMRPhenomPv2",
    reference_frequency=50.0,
    minimum_frequency=20.0,
    catch_waveform_errors=True,
)

waveform_generator_srashti = bilby.gw.WaveformGenerator(
    duration=duration,
    sampling_frequency=sampling_frequency,
    frequency_domain_source_model=test_lensing_tgr.ModelGenericBeyondGRLensingBBH,
    parameter_conversion=bilby.gw.conversion.convert_to_lal_binary_black_hole_parameters,
    waveform_arguments=waveform_arguments,
)

waveform_generator_aditya = bilby.gw.WaveformGenerator(
    duration=duration,
    sampling_frequency=sampling_frequency,
    frequency_domain_source_model=test_lensing_tgr.ModelGenericBeyondGRLensingBBHAditya,
    parameter_conversion=bilby.gw.conversion.convert_to_lal_binary_black_hole_parameters,
    waveform_arguments=waveform_arguments,
)

wf_srashti, wf_aditya = waveform_generator_srashti.frequency_domain_strain(
    params
), waveform_generator_aditya.frequency_domain_strain(params)

for pol in ["plus", "cross"]:
    try:
        np.testing.assert_allclose(wf_srashti[pol], wf_aditya[pol])
        print(f"test passes for {pol} polarization")
    except Exception:
        print(f"test does not pass for {pol} polarization")
        f_srashti = waveform_generator_srashti.frequency_array
        f_aditya = waveform_generator_aditya.frequency_array

        plt.figure()
        plt.loglog(f_srashti, np.abs(wf_srashti[pol]), label="Srashti")
        plt.loglog(f_aditya, np.abs(wf_aditya[pol]), label="Aditya")
        plt.legend()
        plt.savefig(f"amp_comparison_{pol}.jpg", dpi=300)
        plt.close()

        plt.figure()
        plt.plot(f_srashti, np.unwrap(np.angle(wf_srashti[pol])), label="Srashti")
        plt.plot(f_aditya, np.unwrap(np.angle(wf_aditya[pol])), label="Aditya")
        plt.legend()
        plt.savefig(f"phase_comparison_{pol}.jpg", dpi=300)
        plt.close()

        print(f"saved diagnostic plots for {pol} polarization")

