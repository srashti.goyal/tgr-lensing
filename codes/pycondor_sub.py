from pycondor import Job
import os

error = os.path.abspath('outdir/condor/error')
output = os.path.abspath('outdir/condor/output')
log = os.path.abspath('outdir/condor/log')
submit = os.path.abspath('outdir/condor/submit')
job = Job(name='gauss_noise_GRinj_nGR_rec_snr20_psi_pol_rec_1', executable='pe_run_include_phi_spins_HM.py',submit=submit,error=error,\
          output=output, log=log)
job.build_submit()