"""
Script to automate Bilby preliminary PE runs for IMRCT analysis

Apratim Ganguly, Aditya Vijaykumar, 2021-03-07
"""

# initialize required packages
import numpy as np
import pandas as pd
import yaml
from argparse import ArgumentParser
import os, glob
from shutil import copyfile
from datetime import date
from bilby_pipe.main import parse_args
from bilby_pipe.parser import create_parser
import getpass
from bilby.gw.prior import PriorDict
from bilby.core.prior import Uniform

username = getpass.getuser()


def create_analysis_parser():
    return create_parser(top_level=False)


# ~~ Read command line arguments
parser = ArgumentParser()
parser.add_argument("-r", "--rundir", dest="run_dir", help="main run directory path")
parser.add_argument("-w", "--webdir", dest="web_dir", help="main web directory path")
parser.add_argument(
    "-p", "--pe_tag", dest="pe_tag", help="PE tag. Eg. Prod1, Prod2 etc."
)
parser.add_argument("--run_on_events", dest="run_on_events", nargs="+")
parser.add_argument(
    "-s", "--sampler_kwargs", dest="sampler_kwargs", help="sampler kwargs", default=None
)
parser.add_argument(
    "--o3a",
    dest="o3a",
    help="Is this an O3a event?",
    default=None,
    action="store_true",
)
parser.add_argument(
    "--submit",
    dest="submit",
    help="Submit the run?",
    default=None,
    action="store_true",
)

options = parser.parse_args()
run_dir = options.run_dir
web_dir = options.web_dir
pe_tag = options.pe_tag
accounting_tag = "ligo.prod.o3.cbc.testgr.tiger"
sampler_kwargs = options.sampler_kwargs

# ~~ Ledger file

ledger_file = "real_events/O3bledger.yaml"
if options.o3a:
    ledger_file = "real_events/O3aledger.yaml"

with open(ledger_file, "r") as lf:
    ledger_info = yaml.safe_load(lf)

# ~~ Dictionary for the index of each event
dict_of_names = dict()
for i in range(len(ledger_info)):
    dict_of_names[ledger_info[i]["name"]] = i
datetag = date.today().isoformat()
datetag = datetag.replace("-", "")
parser_for_bilby = create_analysis_parser()

for myevent in options.run_on_events:
    print(myevent + ":")

    run_path = f"{run_dir}/{myevent}/{datetag}/"
    web_path = f"{web_dir}/{myevent}/{datetag}/"

    os.system(f"mkdir -p {run_path}")

    if options.o3a:
        ID = int(pe_tag.split("ProdF")[1])
    else:
        ID = int(pe_tag.split("Prod")[1])
    path_to_prod_config = glob.glob(
        ledger_info[dict_of_names[myevent]]["productions"][ID][pe_tag]["rundir"]
        + "/"
        + f"*{pe_tag}*.ini"
    )[0]

    os.chdir(run_path)
    configfile = f"config.ini"
    priorfile = f"prior"
    copyfile(path_to_prod_config, configfile)

    args, unknown_args = parse_args([configfile], parser_for_bilby)

    prior_dictionary = PriorDict()
    prior_dictionary.from_file(args.prior_file)

    for name in ["chirp_mass", "mass_ratio"]:
        if "bilby.gw.prior." not in prior_dictionary[name].__class__.__name__:
            prior_dictionary[name].__class__.__name__ = (
                "bilby.gw.prior." + prior_dictionary[name].__class__.__name__
            )

    prior_dictionary["t12"] = Uniform(minimum=-0.1, maximum=0.1)
    prior_dictionary["lens_angle"] = Uniform(minimum=0, maximum=np.pi/2)

    prior_dictionary.to_file(outdir=".", label=priorfile)

    args.injection_numbers = [None]
    args.label = f"{myevent}_{pe_tag}"
    args.outdir = "outdir"
    args.webdir = web_path
    args.prior_file = f"{priorfile}.prior"
    args.frequency_domain_source_model = (
        "tgrlensing.waveform_model.ModelGenericBeyondGRLensingBBH"
    )
    args.conversion_function = (
        "bilby.gw.conversion.convert_to_lal_binary_black_hole_parameters"
    )
    args.generation_function = "bilby.gw.conversion.generate_all_bbh_parameters"
    args.phase_marginaliztion = False
    args.time_marginalization = False
    args.create_summary = True
    args.summarypages_arguments = dict(no_ligo_skymap=True)
    args.accounting = accounting_tag
    args.distance_marginalization_lookup_table = None

    if options.sampler_kwargs is not None:
        args.sampler_kwargs = f"{{ {sampler_kwargs} }}"

    args.email = f"{username}@ligo.org"

    parser_for_bilby.write_to_file(filename=configfile, args=args, overwrite=True)
    if options.submit:
        os.system("bilby_pipe config.ini --submit")
        print("run launched...")
print("\nAll runs done!")

