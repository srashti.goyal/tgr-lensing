#!/home1/srashti.goyal/anaconda3/envs/bilby_env/bin/python

import bilby
import numpy as np
import matplotlib.pylab as plt
import json
import os
from tgrlensing import waveform_model


t12=0.0
phi=0.

seed=88888881
np.random.seed(seed) #optional
print('seed = ',seed)

#injection_parameters=dict(mass_1 = 36., mass_2 = 29., luminosity_distance = 1500.,dec = -1.2108, ra = 1.375, theta_jn = 0.4, chi_1 = 0.374, chi_2 = 0.321, psi = 2.659, phase = 1.3,geocent_time = 1126259642.413,t12= t12,lens_angle= phi)

## GW150914 type injection maxL https://bilby-gwtc1.github.io/GW150914/html/bilby_bilby.html
injection_parameters=dict(mass_1 = 38.3, mass_2 = 33.19, luminosity_distance = 2*558.553,dec = -1.223, ra = 2.269, theta_jn = 2.921, chi_1 = 0.3, chi_2 = 0.27, psi = 1.575, phase = 1.893,geocent_time = 1126259462.414,t12= t12,lens_angle= phi)

psd_dict={'H1':'/home1/aditya.vijaykumar/work/bilby_bhmimickers/GW150914/h1_psd.dat', 'L1':'/home1/aditya.vijaykumar/work/bilby_bhmimickers/GW150914/l1_psd.dat'}


duration = 8.
sampling_frequency = 2048.

outdir = 'outdir/bug_fixed_GW150914_like_psi_phase_rec'
label = 'gauss_noise_GRinj_nGR_rec_snr20_'+str(seed)
bilby.core.utils.setup_logger(outdir=outdir, label=label)

waveform_arguments = dict(waveform_approximant='IMRPhenomXHM',
                          reference_frequency=20., minimum_frequency=20.)

waveform_generator = bilby.gw.WaveformGenerator(
    duration=duration, sampling_frequency=sampling_frequency,
    frequency_domain_source_model=waveform_model.ModelGenericBeyondGRLensingBBH,parameter_conversion = bilby.gw.conversion.convert_to_lal_binary_black_hole_parameters,waveform_arguments=waveform_arguments)

ifos1 = bilby.gw.detector.InterferometerList(['H1', 'L1','V1'])

#ifos1.set_strain_data_from_zero_noise(sampling_frequency=sampling_frequency, duration=duration,start_time=injection_parameters['geocent_time'] - 6)

ifos1.set_strain_data_from_power_spectral_densities(sampling_frequency=sampling_frequency, duration=duration,start_time=injection_parameters['geocent_time'] - 6)


ifos1.inject_signal(waveform_generator=waveform_generator,parameters=injection_parameters)



# setup the priors
Priors = bilby.gw.prior.BBHPriorDict()
Priors['chirp_mass'] = bilby.core.prior.Uniform(name = 'chirp_mass', minimum = 12., maximum = 80.)
Priors['mass_ratio'] = bilby.core.prior.Uniform(name = 'mass_ratio', minimum = 0.1, maximum = 1.)
Priors['mass_1'] = bilby.core.prior.Constraint(name = 'mass_1', minimum = 5., maximum = 100.)
Priors['mass_2'] = bilby.core.prior.Constraint(name = 'mass_2', minimum = 5.,maximum = 100.)
Priors['chi_1'] = bilby.gw.prior.AlignedSpin(name = 'chi_1', a_prior = bilby.core.prior.Uniform(minimum = 0., maximum = 0.99))
Priors['chi_2'] = bilby.gw.prior.AlignedSpin(name = 'chi_2', a_prior = bilby.core.prior.Uniform(minimum = 0., maximum = 0.99))
Priors['luminosity_distance'] = bilby.gw.prior.UniformComovingVolume(name = 'luminosity_distance',latex_label = '$D_{l}$', minimum = 100., maximum = 5e4)
Priors['theta_jn'] = bilby.core.prior.Sine(name = 'theta_jn', latex_label = '$\iota$', minimum = 0., maximum = np.pi)
Priors['geocent_time'] = bilby.core.prior.Uniform(minimum = injection_parameters['geocent_time'] - 1,maximum = injection_parameters['geocent_time'] + 1, name = 'geocent_time', latex_label = '$t_c$', unit = '$s$')
Priors['ra'] = bilby.prior.Uniform(name = 'ra', latex_label = '$RA$', minimum = 0., maximum = 2*np.pi)
Priors['dec'] = bilby.prior.Cosine(name = 'dec', latex_label = '$dec$', minimum = - np.pi/2., maximum = np.pi/2. )

### nGR recovery
Priors['t12']=bilby.core.prior.Uniform(-0.1,0.1,name='Delta t_12', latex_label='$\Delta t_{12}$', unit='$Msol$')
Priors['lens_angle']=bilby.core.prior.Uniform(0,np.pi/2,name='lens angle', latex_label='$\Phi_{lens}$', unit='$rad$')

### GR recovery
#Priors['t12']=0
#Priors['lens_angle']=0

likelihood = bilby.gw.GravitationalWaveTransient(
    interferometers=ifos1, waveform_generator=waveform_generator,distance_marginalization=True)

# Run sampler.  In this case we're going to use the `dynesty` sampler
result = bilby.run_sampler(
    likelihood=likelihood, priors=Priors, sampler='dynesty',
    injection_parameters=injection_parameters, outdir=outdir, label=label,npoints=1000,nact=20,npool=16) 
### nact change to 20, nlive 1000, chains in dynesty?

truths = injection_parameters.copy()
truths['mass_ratio'] = bilby.gw.conversion.component_masses_to_mass_ratio(truths['mass_1'],truths['mass_2'])
truths['chirp_mass'] = bilby.gw.conversion.component_masses_to_chirp_mass(truths['mass_1'],truths['mass_2'])
truths.pop('mass_1')
truths.pop('mass_2')
# plot the results
result.plot_corner(injection_parameters=truths)
