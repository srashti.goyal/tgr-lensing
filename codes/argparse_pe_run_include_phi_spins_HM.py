#!/home1/aditya.vijaykumar/src/miniconda3/envs/lensingphase/bin/python
###!/home1/srashti.goyal/anaconda3/envs/bilby_env/bin/python

import bilby
import numpy as np
import matplotlib.pylab as plt
import json
import os
from tgrlensing import waveform_model
import argparse
import seaborn as sns

parser = argparse.ArgumentParser(description='This is stand alone code for doing PE on injection with tgr-lensing.')
parser.add_argument(
        '-t12_inj',
        '--t12_inj',
        type=float,
        help='input t12 i.e. birefringence time delay b/w h+ and hx',
        default=0.0)
parser.add_argument(
        '-phi_lens_inj',
        '--phi_lens_inj',
        type=float,
        help='input lens angle i.e. birefringence orientation angle b/w h+ and hx at the lens',
        default=0.0)

parser.add_argument(
        '-seed',
        '--seed',
        type = int,
        help='seed for PE, default: 888888',
        default=None)    

parser.add_argument(
        '-snr_inj',
        '--snr_inj',
        type=int,
        help='snr of the injected signal',
        default=20)

parser.add_argument(
        '-odir',
        '--odir',
        help='output directory: outdir/ODIR',
        default='arparse_test')

parser.add_argument(
        '-label',
        '--label',
        help='label for the run',
        default='test')

parser.add_argument(
        '-gaussian_noise',
        '--gaussian_noise',
        type=int,
        help='add gaussian noise to the injection, default: False',
        default= 0)

parser.add_argument(
        '-nGR_lens_rec',
        '--nGR_lens_rec',
        type=int,
        help='lensing beyond GR recovery: True, GR recovery: False',
        default= 0)
parser.add_argument(
        '-nact',
        '--nact',
        type = int,
        help='nact for PE, default: 20',
        default=20)
parser.add_argument(
        '-npoints',
        '--npoints',
        type = int,
        help='no. of live points for PE, default: 1000',
        default=1000)
parser.add_argument(
        '-npool',
        '--npool',
        type = int,
        help='npool for PE, default: 8',
        default=8)

args = parser.parse_args()
print(args)

t12=args.t12_inj
phi=args.phi_lens_inj

if args.seed == None:
    seed = np.random.randint(8)
else:
    seed=args.seed

np.random.seed(seed) #optional
print('seed = ',seed)

## GW150914 type injection maxL https://bilby-gwtc1.github.io/GW150914/html/bilby_bilby.html
#injection_parameters=dict(mass_1 = 38.3, mass_2 = 33.19, luminosity_distance = 558.553,dec = -1.223, ra = 2.269, theta_jn = 2.921, chi_1 = 0.3, chi_2 = 0.27, psi = 1.575, phase = 1.893,geocent_time = 1126259462.414,t12= t12,lens_angle= phi)

## GW190814 injection more inclined
injection_parameters= dict(mass_1 = 24.45, mass_2 = 2.71, luminosity_distance = 295.18,dec = -0.436, ra = 0.217, theta_jn = 0.8, chi_1 = 0.06, chi_2 = 0.46, psi = 1.575, phase = 4.421,geocent_time = 1249852256.99,t12= t12,lens_angle= phi)


# load PSD files
psd_files={'H1':'psd/aLIGO_ZERO_DET_high_P_psd.txt','L1':'psd/aLIGO_ZERO_DET_high_P_psd.txt','V1':'psd/AdV_psd.txt'}

duration = 12.
sampling_frequency = 2048.

outdir = 'outdir/'+args.odir
label = args.label+'_snr_'+str(args.snr_inj)+'_seed_'+str(seed)
bilby.core.utils.setup_logger(outdir=outdir, label=label)

waveform_arguments = dict(waveform_approximant='IMRPhenomXHM',
                          reference_frequency=20., minimum_frequency=20.)

waveform_generator = bilby.gw.WaveformGenerator(
    duration=duration, sampling_frequency=sampling_frequency, frequency_domain_source_model=waveform_model.ModelGenericBeyondGRLensingBBH, parameter_conversion = bilby.gw.conversion.convert_to_lal_binary_black_hole_parameters,waveform_arguments=waveform_arguments)

ifos1 = bilby.gw.detector.InterferometerList(['H1', 'L1','V1'])

ifos = bilby.gw.detector.InterferometerList(['H1', 'L1','V1']) #for snr rescaling

# add PSDs to detectors from files loaded
for ifo in ifos1:
    ifo.power_spectral_density = \
    bilby.gw.detector.PowerSpectralDensity(psd_file=psd_files[ifo.name])
    
for ifo in ifos:
    ifo.power_spectral_density = \
    bilby.gw.detector.PowerSpectralDensity(psd_file=psd_files[ifo.name])
        
ifos.set_strain_data_from_zero_noise(sampling_frequency=sampling_frequency, duration=duration,start_time=injection_parameters['geocent_time'] - 6)

ifos.inject_signal(waveform_generator=waveform_generator,parameters=injection_parameters)


snr_pars=np.sqrt(np.sum([ifo.optimal_snr_squared(ifo.frequency_domain_strain) for ifo in ifos]))


rescaling = snr_pars/args.snr_inj

print(snr_pars,rescaling)


injection_parameters['luminosity_distance'] *= np.real(rescaling)

if args.gaussian_noise == 0 :
    ifos1.set_strain_data_from_zero_noise(sampling_frequency=sampling_frequency, duration=duration,start_time=injection_parameters['geocent_time'] - 6)

else:
    ifos1.set_strain_data_from_power_spectral_densities(sampling_frequency=sampling_frequency, duration=duration,start_time=injection_parameters['geocent_time'] - 6)


ifos1.inject_signal(waveform_generator=waveform_generator,parameters=injection_parameters)


# setup the priors
Priors = bilby.gw.prior.BBHPriorDict()
Priors['chirp_mass'] = bilby.core.prior.Uniform(name = 'chirp_mass', minimum = 1., maximum = 80.)
Priors['mass_ratio'] = bilby.core.prior.Uniform(name = 'mass_ratio', minimum = 0.05, maximum = 1.)
Priors['mass_1'] = bilby.core.prior.Constraint(name = 'mass_1', minimum = 1., maximum = 100.)
Priors['mass_2'] = bilby.core.prior.Constraint(name = 'mass_2', minimum = 1.,maximum = 100.)
Priors['chi_1'] = bilby.gw.prior.AlignedSpin(name = 'chi_1', a_prior = bilby.core.prior.Uniform(minimum = 0., maximum = 0.99))
Priors['chi_2'] = bilby.gw.prior.AlignedSpin(name = 'chi_2', a_prior = bilby.core.prior.Uniform(minimum = 0., maximum = 0.99))
Priors['luminosity_distance'] = bilby.gw.prior.UniformComovingVolume(name = 'luminosity_distance',latex_label = '$D_{l}$', minimum = 100., maximum = 5e4)
Priors['theta_jn'] = bilby.core.prior.Sine(name = 'theta_jn', latex_label = '$\iota$', minimum = 0., maximum = np.pi)
Priors['geocent_time'] = bilby.core.prior.Uniform(minimum = injection_parameters['geocent_time'] - 1,maximum = injection_parameters['geocent_time'] + 1, name = 'geocent_time', latex_label = '$t_c$', unit = '$s$')
Priors['ra'] = bilby.prior.Uniform(name = 'ra', latex_label = '$RA$', minimum = 0., maximum = 2*np.pi)
Priors['dec'] = bilby.prior.Cosine(name = 'dec', latex_label = '$dec$', minimum = - np.pi/2., maximum = np.pi/2. )

if args.nGR_lens_rec == 1:
    ### nGR recovery
    Priors['t12']=bilby.core.prior.Uniform(-0.1,0.1,name='Delta t_12', latex_label='$\Delta t_{12}$', unit='$Msol$')
    Priors['lens_angle']=bilby.core.prior.Uniform(0,np.pi/2,name='lens angle', latex_label='$\Phi_{lens}$', unit='$rad$')
    
else:

    ### GR recovery
    Priors['t12']=0
    Priors['lens_angle']=0

likelihood = bilby.gw.GravitationalWaveTransient(
    interferometers=ifos1, waveform_generator=waveform_generator, distance_marginalization = True,priors = Priors)

# Run sampler.  In this case we're going to use the `dynesty` sampler
result = bilby.run_sampler(
    likelihood=likelihood, priors=Priors, sampler='dynesty',
    injection_parameters=injection_parameters, outdir=outdir, label=label,npoints=args.npoints,nact=args.nact,npool=args.npool) 

truths = injection_parameters.copy()
truths['mass_ratio'] = bilby.gw.conversion.component_masses_to_mass_ratio(injection_parameters['mass_1'],injection_parameters['mass_2'])
truths['chirp_mass'] = bilby.gw.conversion.component_masses_to_chirp_mass(injection_parameters['mass_1'],injection_parameters['mass_2'])
truths.pop('mass_1')
truths.pop('mass_2')
# plot the results
result.plot_corner(injection_parameters=truths)

if args.nGR_lens_rec == 1:

    par1='t12'
    par2='lens_angle'
    plt.figure()
    sns.jointplot(
        data=result.posterior,
        x=par1, y=par2,
        kind="kde")

    plt.savefig(outdir+'/'+label + 't12_phi_2d.png')