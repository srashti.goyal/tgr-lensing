from setuptools import setup, find_packages

setup(
    name="tgrlensing",
    version="0.1.0",
    install_requires=["bilby"],
    author='Srashti Goyal',
    author_email='srashti.goyal@icts.res.in',
    packages=["tgrlensing"],
    include_package_data=True)
