from pesummary.utils.samples_dict import MultiAnalysisSamplesDict
from pesummary.io import read
import argparse
import numpy as np
import os
import matplotlib.pyplot as plt

parser = argparse.ArgumentParser()

parser.add_argument(
    "--samples", help="Samples separated by a space", nargs="+", required=True
)
parser.add_argument(
    "--labels", help="labels separated by a space", nargs="+", required=True
)
parser.add_argument("--tag", help="tag for the final plot", required=True)
parser.add_argument("--outdir", help="Where to save the plot?", required=True)

args = parser.parse_args()

samples = args.samples
labels = args.labels
tag = args.tag
outdir = args.outdir

os.makedirs(outdir, exist_ok=True)
# params_to_plot = dict(
#    five_params=["chirp_mass", "mass_ratio", "luminosity_distance", "psi", "phase"],
#    seven_params=[
#        "chirp_mass",
#        "mass_ratio",
#        "luminosity_distance",
#        "theta_jn",
#        "dec",
#        "psi",
#        "phase",
#    ],
# )

# params_to_plot = dict(
#    intrinsic_parameters=["chirp_mass", "mass_ratio", "psi", "phase"],
#    extrinsic_parameters=[
#        "luminosity_distance",
#        "theta_jn",
#        "dec",
#        "ra",
#        "geocent_time",
#    ],
# )

params_to_plot = dict(
    intrinsic_parameters=[
        "chirp_mass",
        "mass_ratio",
        "luminosity_distance",
        "iota",
        "chi_eff",
    ],
    extrinsic_parameters=[
	"phase", "psi", "ra", "dec",
]
)


read_kwargs = dict(disable_prior=True, disable_injection_conversion=True)

open_files = dict()
for i in range(len(labels)):
    read_file = read(samples[i], **read_kwargs)
    if i == 0:
        injection_parameters = read_file.injection_parameters
    open_files[labels[i]] = read_file.samples_dict

dict_of_samples = MultiAnalysisSamplesDict(open_files)

for key in params_to_plot.keys():
    injected = [injection_parameters[i] for i in params_to_plot[key]]
    fig = dict_of_samples.plot(
        parameters=params_to_plot[key],
        type="corner",
        truths=injected,
        labels=labels,
        levels=[0.68, 0.95],
    )
    fig.savefig(outdir + f"/{tag}_{key}.jpg", dpi=300)
    fig.tight_layout()
    fig.clf()
