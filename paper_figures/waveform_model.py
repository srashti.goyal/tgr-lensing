"""
File to store the lensing beyond GR waveform with birefringence model.
"""

import numpy as np
from bilby.gw.source import *


def ModelGenericBeyondGRLensingBBH(
    frequency_array,
    mass_1,
    mass_2,
    luminosity_distance,
    a_1,
    tilt_1,
    phi_12,
    a_2,
    tilt_2,
    phi_jl,
    theta_jn,
    phase,
    t12=0.0,
    lens_angle=0,
    **kwargs
):

    gr_polarizations = lal_binary_black_hole(
        frequency_array,
        mass_1,
        mass_2,
        luminosity_distance,
        a_1,
        tilt_1,
        phi_12,
        a_2,
        tilt_2,
        phi_jl,
        theta_jn,
        phase,
        **kwargs
    )
    Delta = np.exp(1j * frequency_array * 2 * np.pi * t12)
    hp, hc = gr_polarizations["plus"], gr_polarizations["cross"]
    cos_term = np.cos(4 * lens_angle)
    sin_term = np.sin(4 * lens_angle)

    waveform_polarizations = dict()

    waveform_polarizations["plus"] = (
        1 / 2.0 * ((Delta - 1.0) * cos_term + Delta + 1) * hp
        + 1 / 2.0 * (Delta - 1.0) * sin_term * hc
    )
    waveform_polarizations["cross"] = (
        1 / 2.0 * (-(Delta - 1.0) * cos_term + Delta + 1) * hc
        + 1 / 2.0 * (Delta - 1.0) * sin_term * hp
    )

    return waveform_polarizations
