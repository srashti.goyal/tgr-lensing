"Functions for computing the GW mixing in the limit of small radius"

import numpy as np
import scipy.integrate as integrate
from scipy.interpolate import griddata

from background_spherical_horndeski import read_params, r4_Mpc, rs_Mpc, H0_iMpc, rV_Mpc
    
"Scalar field derivatives"

def dphibar_small(r_r4,c4phi,c4X,c4XX):
    coeff0 = -0.5*(c4phi/(c4XX+2.*c4X**2))**(1./3)
    #coeff1 = c4phi*c4X/(c4XX+2*c4X**2)
    return coeff0 + np.zeros(len(r_r4))#coeff1 * r_r4

def ddphibar_small(r_r4,c2X,c4phi,c4X,c4XX):
    dphi = -0.5*(c4phi/(c4XX+2.*c4X**2))**(1./3)#dphibar_small(r_r4,c4phi,c4X,c4XX)
    x3 = (8.)*(c4XX + 2.*c4X**2.)
    x2 = (4.*r_r4)*(-6.*c4X*c4phi)
    x1 = (2.*r_r4**2.)*(c2X + 3.*c4phi**2)
    dx2 = (4.)*(-6.*c4X*c4phi)
    dx1 = (4.*r_r4)*(c2X + 3.*c4phi**2)
    return -(dx2*dphi**2 + dx1*dphi)/(3.*x3 * dphi**2 +2.* x2*dphi + x1)

def ddphibar_small_const(c2X,c4phi,c4X,c4XX):
    dphi = -0.5*(c4phi/(c4XX+2.*c4X**2))**(1./3)
    x3 = (8.)*(c4XX + 2.*c4X**2.)
    x2 = 0#(4.*r_r4)*(-6.*c4X*c4phi)
    x1 = 0#(2.*r_r4**2.)*(c2X + 3.*c4phi**2)
    dx2 = (4.)*(-6.*c4X*c4phi)
    dx1 = 0.#(4.*r_r4)*(c2X + 3.*c4phi**2)
    return -(dx2*dphi**2 + dx1*dphi)/(3.*x3 * dphi**2 +2.* x2*dphi + x1)
    
"Mixing Upsilon"
def Upsilon_can_small(r_r4, params,rsr4,r2r4=1.,m=1,theta=np.pi/2.,across_line=False):
    '''kinetic mixing coeficient
       r_r4 -> array for radial values (units of r_4)
       params -> theory parameters
       rsr4 -> r_s/r_4
       r2r4 -> Lambda_4/Lambda_2
       m -> mass profile
       theta -> angle with propagation (array or number)
    '''
    
    c2, c4phi, c4X, c4XX = read_params(params) 
    
    dphi_bar = dphibar_small(r_r4,c4phi,c4X,c4XX)
    ddphi_bar = ddphibar_small(r_r4,1.,c4phi,c4X,c4XX)
    
    #Sum 4.*n*c4Xn*(rSrV**(n-1.))*(dphi_bar**(2.*(n-1)))
    Upsilon = 0.
    for (n,c4Xn) in [(1,c4X),(2,c4XX)]:
        Upsilon += 4.*n*c4Xn*(rsr4**(n-1.))*(dphi_bar**(2.*(n-1)))
    Upsilon *= (ddphi_bar-dphi_bar/r_r4)
    
    Norm = 1
    if c2 !=0:
        Norm = (1+4*c2*dphi_bar**2*r2r4**4)
        print('need to fix r2r4 in Upsilon_can!')
    
    #multiply radial and angular dependences
    radial_dep = Upsilon/Norm
    angular_dep = np.sin(theta)**2 #no angular dependence
    
    if (across_line == True):
        return radial_dep*angular_dep
    else:
        return np.outer(radial_dep,angular_dep)

"Velocities"
def Delta_ch2_small(r_r4, params,rsr4,m=1,theta=0,across_line=False):
    '''GW field velocity before mixing
       r_r4 -> array for radial values (units of r_4)
       params -> theory parameters
       rsr4 -> r_s/r_4
       r2r4 -> Lambda_4/Lambda_2
       m -> mass profile
       theta -> angle with propagation (array or number)
       across_line -> if r_r4 and theta represent a trajectory
    '''
    
    c2, c4phi, c4X, c4XX = read_params(params) 
    
    dphi_bar = dphibar_small(r_r4,c4phi,c4X,c4XX)
    #ddphi_bar = ddphibar_small(r_r4,1.,c4phi,c4X,c4XX)
    
    #Sum 4.*n*c4Xn*(rSrV**(n-1.))*(dphi_bar**(2.*(n-1)))
    G4X = 0.
    G4 = 1.
    for (n,c4Xn) in [(1,c4X),(2,c4XX)]:
        G4X += -4.*n*c4Xn*(rsr4**(n-1.))*(dphi_bar**(2.*(n-1)))
        G4 += 2.*c4Xn*(rsr4**n)*(dphi_bar**(2.*n))
    
    #multiply radial and angular dependences dphi_\parallel = dphi cos(th)
    radial_dep = G4X/G4*rsr4*dphi_bar**2
    angular_dep = np.cos(theta)**2
    
    if (across_line == True):
        return radial_dep*angular_dep
    else:
        return np.outer(radial_dep,angular_dep)
    
def Delta_cs2_small(r_r4, params,r2r4,m=1,theta=0,across_line=False):
    '''scalar velocity before mixing
       r_r4 -> array for radial values (units of r_4)
       params -> theory parameters
       rsr4 -> r_s/r_4
       r2r4 -> Lambda_4/Lambda_2
       m -> mass profile
       theta -> angle with propagation (array or number)
       across_line -> if r_r4 and theta represent a trajectory
    '''
    
    c2, c4phi, c4X, c4XX = read_params(params) 
    
    dphi_bar = dphibar_small(r_r4,c4phi,c4X,c4XX)
    #ddphi_bar = ddphibar_small(r_r4,1.,c4phi,c4X,c4XX)
    
    #Sum 4.*n*c4Xn*(rSrV**(n-1.))*(dphi_bar**(2.*(n-1)))
    G2XX = 2.*c2*r2r4*dphi_bar**2
    G2X = 1. - c2*r2r4*dphi_bar**2
    
    #multiply radial and angular dependences
    radial_dep = G2XX/G2X
    angular_dep = np.cos(theta)**2
    
    if (across_line == True):
        return radial_dep*angular_dep
    else:
        return np.outer(radial_dep,angular_dep)

def Delta_c22_small(r_r4, params,r2r4,m=1,theta=0,across_line=False):
    Ups_s = Upsilon_can_small(r_r4,params,rsr4=(rs/r4), theta=theta,m=m,across_line=True)
    Dch2_s = Delta_ch2_small(r_r4,params,rsr4=(rs/r4), theta=theta,m=m,across_line=True)
    Dcs2_s = Delta_cs2_small(r_r4,params,r2r4=(L4/L2), theta=theta,m=m,across_line=True)
    return Ups_s**2*(Dch2_s)**2/(Dch2_s-Dcs2_s)
    
"Time delays"

def Dt_r4_10_analytic(b,c2XX, c4phi, c4X, c4XX,L4,M,z_max):
    #c2XX, c4phi, c4X, c4XX = read_params(params) 
    #L4 = 1/np.sqrt(abs(c4X))
    r4 = r4_Mpc(L4=L4,M=M)
    rs = rs_Mpc(M=M)
    rSr4 = rs/r4
    
    dphibardr = -0.5*(c4phi/(c4XX+2.*c4X**2))**(1./3)
    
    return 4.*rSr4*c4X*dphibardr**2*(z_max-b*np.arctan(z_max/b))

def Dt_r4_21_analytic(b,c2XX, c4phi, c4X, c4XX,L4,M,z):
    # z is the maxim z in the integration
    #c2X, c4phi, c4X, c4XX = read_params(params) 
    #L4 = 1/np.sqrt(abs(c4X))
    r4 = r4_Mpc(L4=L4,M=M)
    rs = rs_Mpc(M=M)
    rSr4 = rs/r4
    r_r4 = np.sqrt(b**2 + z**2)
    r2 = b**2 + z**2
    
    C0 = 4*c4X
    C1 = ddphibar_small(r_r4,1.,c4phi,c4X,c4XX)
    C2 = -0.5*(c4phi/(c4XX+2.*c4X**2))**(1./3)#dphibar_small(r_r4,c4phi,c4X,c4XX)
    C3 = -4* c4X * rSr4 * C2**2.
    
    p1 = -(b*z/r_r4**7)*(30*b**6*C1**2*r_r4+C2*z**4*(64*C1*z**2 - 15*C2*r_r4)+5*b**4*C2*(32*C1*z**2+3*C2*r_r4)+b**2*(224*C1*C2*z**4-40*C2**2*z**2*r_r4-30*C1**2*z**4*r_r4))
    p2 = 15*(2*b**2*C1**2+C2**2)*np.arctan(z/b)
    result = (p1+p2)*C3*C0**2/240/b
    
    return -result

def Dt_r4_21_analytic_slope(b,c2XX, c4phi, c4X, c4XX,L4,M,z):
    # z is the maxim z in the integration
    #c2X, c4phi, c4X, c4XX = read_params(params) 
    #L4 = 1/np.sqrt(abs(c4X))
    r4 = r4_Mpc(L4=L4,M=M)
    rs = rs_Mpc(M=M)
    rSr4 = rs/r4
    r_r4 = np.sqrt(b**2 + z**2)
    r2 = b**2 + z**2
    
    C0 = 4*c4X
    C1 = ddphibar_small(r_r4,1.,c4phi,c4X,c4XX)
    C2 = -0.5*(c4phi/(c4XX+2.*c4X**2))**(1./3)#dphibar_small(r_r4,c4phi,c4X,c4XX)
    C3 = -4* c4X * rSr4 * C2**2.
    
    #p1 = -(b*z/r_r4**7)*(30*b**6*C1**2*r_r4+C2*z**4*(64*C1*z**2 - 15*C2*r_r4)+5*b**4*C2*(32*C1*z**2+3*C2*r_r4)+b**2*(224*C1*C2*z**4-40*C2**2*z**2*r_r4-30*C1**2*z**4*r_r4))
    #p2 = 15*(2*b**2*C1**2+C2**2)*np.arctan(z/b)
    #p2 = 15*(C2**2)*np.arctan(z/b)
    #result = (p2)*C3*C0**2/240/b
    result = -c4X**3*(c4phi/(c4XX+2.*c4X**2))**(4/3)*rSr4*np.arctan(z/b)/4/b
    
    return -result

"Deflection angle"
def ddphibar_small_const(c2X,c4phi,c4X,c4XX):
    dphi = -0.5*(c4phi/(c4XX+2.*c4X**2))**(1./3)
    x3 = (8.)*(c4XX + 2.*c4X**2.)
    x2 = 0#(4.*r_r4)*(-6.*c4X*c4phi)
    x1 = 0#(2.*r_r4**2.)*(c2X + 3.*c4phi**2)
    dx2 = (4.)*(-6.*c4X*c4phi)
    dx1 = 0.#(4.*r_r4)*(c2X + 3.*c4phi**2)
    return -(dx2*dphi**2 + dx1*dphi)/(3.*x3 * dphi**2 +2.* x2*dphi + x1)

def alpha_10_analytic(b,c2XX, c4phi, c4X, c4XX,L4,M,z):
    #c2XX, c4phi, c4X, c4XX = read_params(params) 
    #L4 = 1/np.sqrt(abs(c4X))
    r4 = r4_Mpc(L4=L4,M=M)
    rs = rs_Mpc(M=M)
    rSr4 = rs/r4
    r_r4 = np.sqrt(b**2 + z**2)
    r2 = b**2 + z**2
    
    dphibardr = -0.5*(c4phi/(c4XX+2.*c4X**2))**(1./3)
    c2X = 1.
    x3 = (8.)*(c4XX + 2.*c4X**2.)
    dx2 = (4.)*(-6.*c4X*c4phi)
    dx1 = (4.)*(c2X + 3.*c4phi**2)
    #ddphibardr2 = ddphibar_small_const(1.,c4phi,c4X,c4XX)
    ddphibardr2_1 = -(dx2*dphibardr**2)/(3.*x3 * dphibardr**2)
    ddphibardr2_2 = -(dx1*dphibardr)/(3.*x3 * dphibardr**2)
    
    coef = 4 * c4X * rSr4 * dphibardr #* ddphibardr2
    
    p1 = ddphibardr2_1*(-z + r_r4 * np.arcsinh(z/b)) /r_r4
    p2 = ddphibardr2_2*(z - b * np.arctan(z/b))
    
    return 2. * coef * b *(p1 + p2)
valpha_10_analytic = np.vectorize(alpha_10_analytic)

def alpha_21_analytic(b,c2XX, c4phi, c4X, c4XX,L4,M,z):
    #c2XX, c4phi, c4X, c4XX = read_params(params) 
    #L4 = 1/np.sqrt(abs(c4X))
    r4 = r4_Mpc(L4=L4,M=M)
    rs = rs_Mpc(M=M)
    rSr4 = rs/r4
    r_r4 = np.sqrt(b**2 + z**2)
    r2 = b**2 + z**2
    
    dphibardr = -0.5*(c4phi/(c4XX+2.*c4X**2))**(1./3)
    c2X = 1.
    
    C1 = 4.*c4X * dphibardr
    C2 = -4. * c4X * rSr4 * dphibardr**2
    
    return 2.* C1**2 *C2 * z**3 *(35*b**4 + 28*b**2 *z**2 + 8*z**4)/105/b**2/r_r4**7
valpha_21_analytic = np.vectorize(alpha_21_analytic)

