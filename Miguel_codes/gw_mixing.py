"Functions for computing the GW mixing in the limit of small radius"

import numpy as np
import scipy.integrate as integrate
from scipy.interpolate import griddata

from background_spherical_horndeski import read_params, r4_Mpc, rs_Mpc, H0_iMpc, rV_Mpc
    
"Scalar field derivatives"

def dphi_bar_sol(r,param):
    c2X, c4phi, c4X, c4XX = param
    x3 = (8.)*(c4XX + 2.*c4X**2.)
    x2 = (4.*r)*(-6.*c4X*c4phi)
    x1 = (2.*r**2.)*(c2X + 3.*c4phi**2)
    x0 = c4phi 
    coeff_eq = [x3,x2,x1,x0]
    return np.roots(coeff_eq)
vdphi_bar_sol = np.vectorize(dphi_bar_sol)

def ddphi_bar_sol(r,dphi,param):
    c2, c4phi, c4X, c4XX = param
    x3 = (8.)*(c4XX + 2.*c4X**2.)
    x2 = (4.*r)*(-6.*c4X*c4phi)
    x1 = (2.*r**2.)*(c2 + 3.*c4phi**2)
    dx2 = (4.)*(-6.*c4X*c4phi)
    dx1 = (4.*r)*(c2 + 3.*c4phi**2)
    return -(dx2*dphi**2 + dx1*dphi)/(3.*x3 * dphi**2 +2.* x2*dphi + x1)

def dphibar(r,c2X,c4phi,c4X,c4XX):

    params = [c2X, c4phi, c4X, c4XX]

    if r < 100 :
        dphi1_sol, dphi2_sol, dphi3_sol = dphi_bar_sol(r,params)
        dphi_bar = np.real(dphi3_sol)
    else:
        dphi1_sol, dphi2_sol, dphi3_sol = dphi_bar_sol(100.,params)
        dphi_bar = np.real(dphi3_sol)*(100./r)**2.
    return dphi_bar
vdphibar = np.vectorize(dphibar)

def ddphibar(r,c2X,c4phi,c4X,c4XX):

    params = [c2X, c4phi, c4X, c4XX]

    #x1_sol, x2_sol, x3_sol = x_sol(r,params)
    dphi1_sol, dphi2_sol, dphi3_sol = dphi_bar_sol(r,params)

    dphi_bar = np.real(dphi3_sol)
    return ddphi_bar_sol(r,dphi_bar,params)
vddphibar = np.vectorize(ddphibar)

"Upsilon"
def upsilon(r_r4,c2XX, c4phi, c4X, c4XX,rsr4,r2r4=1.,m=1,theta=np.pi/2.,across_line=False):
    '''kinetic mixing coeficient
       r_r4 -> array for radial values (units of r_4)
       params -> theory parameters
       rsr4 -> r_s/r_4
       r2r4 -> Lambda_4/Lambda_2
       m -> mass profile
       theta -> angle with propagation (array or number)
    '''
        
    dphi_bar = vdphibar(r_r4,1.,c4phi,c4X,c4XX)
    ddphi_bar = vddphibar(r_r4,1.,c4phi,c4X,c4XX)
    
    #Sum 4.*n*c4Xn*(rSrV**(n-1.))*(dphi_bar**(2.*(n-1)))
    Upsilon = 0.
    for (n,c4Xn) in [(1,c4X),(2,c4XX)]:
        Upsilon += 4.*n*c4Xn*(rsr4**(n-1.))*(dphi_bar**(2.*(n-1)))
    Upsilon *= (ddphi_bar-dphi_bar/r_r4)
    
    Norm = 1
    if c2XX !=0:
        Norm = (1+4*c2XX*dphi_bar**2*r2r4**4)
        print('need to fix r2r4 in Upsilon_can!')
    
    #multiply radial and angular dependences
    radial_dep = Upsilon/Norm
    angular_dep = np.sin(theta)**2 #no angular dependence
    
    if (across_line == True):
        return radial_dep*angular_dep
    else:
        return np.outer(radial_dep,angular_dep)

"Time delays"
from gw_mixing_small_r_limit import Dt_r4_10_analytic, Dt_r4_21_analytic
from background_spherical_horndeski import Delta_ch2, Delta_cs2, Upsilon_can

def Dt_r4_10(b_r4,c2XX, c4phi, c4X, c4XX,L4=1,M=1e12,m=1,n_points = 500,z_max=1):
    '''Compute Dt_{10}, the time delay between eigenstate 1 (pure-metric) and EM signals
       given an impact parameter, in units of r4
       b_r4 -> impact parameter in units of r4
       params -> theory parameters
       m -> mass profile
       
       Approximate the integral inside Vainshtein radius with analytical result
    '''
    
    #c2XX, c4phi, c4X, c4XX = read_params(params) 
    params = [c2XX, c4phi, c4X, c4XX]
    #L4 = 1/np.sqrt(abs(c4X))
    L2 = 1 if c2XX == 0 else 1./np.sqrt(abs(c2XX))
    r4 = r4_Mpc(L4=L4,M=M)
    rV = rV_Mpc(L4=L4,M=M,c4phi=c4phi)
    rs = rs_Mpc(M=M)
    
    b_cut = 1e-1*rV/r4
    z_cut = b_cut
    if b_r4 > b_cut:
        z_r4 = np.logspace(min(-5,np.log10(1e-3*z_max),np.log10(1e-3*b_r4)),np.log10(z_max),n_points)
        r_r4 = np.sqrt(z_r4**2 + b_r4**2)
        theta = np.arctan(b_r4/z_r4)
    
        Dch2 = Delta_ch2(r_r4,params,rsr4=(rs/r4), theta=theta,m=m,across_line=True)
    
        #ci = sqrt(cj^2 + Dij) ~ cj + 1/2*Dij/cj + ...
        #Dt = int_{-inf}^inf dz(1/ci - 1/cj) = 2 int_0^inf dz (1/ci-1/cj) = int_0^inf(-Dij)
        #factor 1/2 from the sqrt cancels with integration from 0-z
        integrand = -Dch2 #*2./2.
        result = integrate.simps(integrand,x=z_r4)
    else:
        if z_max < z_cut:
            result = Dt_r4_10_analytic(b_r4,c2XX, c4phi, c4X, c4XX,L4,M,z_max)
        else:
            integral_analytic = Dt_r4_10_analytic(b_r4,c2XX, c4phi, c4X, c4XX,L4,M,z_cut)
            z_r4 = np.logspace(np.log10(z_cut),np.log10(z_max),n_points)
            r_r4 = np.sqrt(z_r4**2 + b_r4**2)
            theta = np.arctan(b_r4/z_r4)
    
            Dch2 = Delta_ch2(r_r4,params,rsr4=(rs/r4), theta=theta,m=m,across_line=True)
    
            integrand = -Dch2 #*2./2.
            integral_numeric = integrate.simps(integrand,x=z_r4)
            result = integral_analytic + integral_numeric
    
    return result
vDt_r4_10 = np.vectorize(Dt_r4_10)

def Dt_r4_21(b_r4,c2XX, c4phi, c4X, c4XX,L4=1,M=1e12,m=1,n_points = 500,z_max=1):
    '''Compute Dt_{10}, the time delay between eigenstate 1 (pure-metric) and EM signals
       given an impact parameter, in units of r4
       b_r4 -> impact parameter in units of r4
       params -> theory parameters
       m -> mass profile
       
       Approximate the integral inside Vainshtein radius with analytical result
    '''
    
    #c2XX, c4phi, c4X, c4XX = read_params(params)
    params = [c2XX, c4phi, c4X, c4XX]
    #L4 = 1/np.sqrt(abs(c4X))
    L2 = 1 if c2XX == 0 else 1./np.sqrt(abs(c2XX))
    r4 = r4_Mpc(L4=L4,M=M)
    rV = rV_Mpc(L4=L4,M=M,c4phi=c4phi)
    rs = rs_Mpc(M=M)
    
    b_cut = 1e-1*rV/r4
    z_cut = b_cut
    if b_r4 > b_cut:
        z_r4 = np.logspace(min(-5,np.log10(1e-3*z_max),np.log10(1e-3*b_r4)),np.log10(z_max),n_points)
        r_r4 = np.sqrt(z_r4**2 + b_r4**2)
        theta = np.arctan(b_r4/z_r4)
        
        Ups = Upsilon_can(r_r4,params,rsr4=(rs/r4), theta=theta,m=m,across_line=True)
        Dch2 = Delta_ch2(r_r4,params,rsr4=(rs/r4), theta=theta,m=m,across_line=True)
        Dcs2 = Delta_cs2(r_r4,params,r2r4=(L4/L2), theta=theta,m=m,across_line=True)
    
        #exact results, don't work well
        #Sigma = (2.+Dch2+Dcs2-2.*Ups**2)/(1-Ups**2)
        #Delta = np.sqrt((Dch2-Dcs2)**2+4.*Ups**2*(Dch2)*(Dcs2))/(1-Ups**2)
        #c22 = (Sigma+Delta)/2.
        #c32 = (Sigma-Delta)/2.

        Dc22_exp = Ups**2*(Dch2)**2/(Dch2-Dcs2)
    
        #ci = sqrt(cj^2 + Dij) ~ cj + 1/2*Dij/cj + ...
        #Dt = int_{-inf}^inf dz(1/ci - 1/cj) = 2 int_0^inf dz (1/ci-1/cj) = int_0^inf(-Dij)
        #factor 1/2 from the sqrt cancels with integration from 0-z
        integrand = -Dc22_exp 
        result = integrate.simps(integrand,x=z_r4)
    else:
        if z_max < z_cut:
            result = Dt_r4_21_analytic(b_r4,c2XX, c4phi, c4X, c4XX,L4,M,z_max)
        else:
            integral_analytic = Dt_r4_21_analytic(b_r4,c2XX, c4phi, c4X, c4XX,L4,M,z_cut)
            z_r4 = np.logspace(np.log10(z_cut),np.log10(z_max),n_points)
            r_r4 = np.sqrt(z_r4**2 + b_r4**2)
            theta = np.arctan(b_r4/z_r4)
    
            Ups = Upsilon_can(r_r4,params,rsr4=(rs/r4), theta=theta,m=m,across_line=True)
            Dch2 = Delta_ch2(r_r4,params,rsr4=(rs/r4), theta=theta,m=m,across_line=True)
            Dcs2 = Delta_cs2(r_r4,params,r2r4=(L4/L2), theta=theta,m=m,across_line=True)
    
            Dc22_exp = Ups**2*(Dch2)**2/(Dch2-Dcs2)
            integrand = -Dc22_exp 
            integral_numeric = integrate.simps(integrand,x=z_r4)
            result = integral_analytic + integral_numeric
    
    return result
vDt_r4_21 = np.vectorize(Dt_r4_21)

"Deflection angle"
from gw_mixing_small_r_limit import alpha_10_analytic, alpha_21_analytic

def dDch2_radial_dr(r_r4,c2XX, c4phi, c4X, c4XX,L4=1,M=1e12,m=1):
    "Valid for linear theory"
    
    #c2XX, c4phi, c4X, c4XX = read_params(params) 
    L2 = 1 if c2XX == 0 else 1./np.sqrt(abs(c2XX))
    r4 = r4_Mpc(L4=L4,M=M)
    rs = rs_Mpc(M=M)
    rSr4 = rs / r4
    
    dphi_barr = vdphibar(r_r4,1.,c4phi,c4X,c4XX)
    ddphi_barr = vddphibar(r_r4,1.,c4phi,c4X,c4XX)
    
    return -8. * c4X * rSr4 * dphi_barr * ddphi_barr
vdDch2_radial_dr = np.vectorize(dDch2_radial_dr)

def alpha_10_num(b_r4,c2XX, c4phi, c4X, c4XX,L4=1,M=1e12,m=1,n_points = 500,z_max=1):
    '''Compute alpha_{10}, relative deflection angle between eigenstate 1 (pure-metric) and EM signals
       given an impact parameter, in units of r4
       b_r4 -> impact parameter in units of r4
       params -> theory parameters
       m -> mass profile
    '''
    
    #c2XX, c4phi, c4X, c4XX = read_params(params) 
    params = [c2XX, c4phi, c4X, c4XX]
    L2 = 1 if c2XX == 0 else 1./np.sqrt(abs(c2XX))
    r4 = r4_Mpc(L4=L4,M=M)
    rs = rs_Mpc(M=M)
    
    z_r4 = np.logspace(min(-5,np.log10(1e-3*z_max),np.log10(1e-3*b_r4)),np.log10(z_max),n_points)
    r_r4 = np.sqrt(z_r4**2 + b_r4**2)
    theta = np.arctan(b_r4/z_r4)
    
    #compute the radial derivative
    Dch2_radial_prime = vdDch2_radial_dr(r_r4,c2XX, c4phi, c4X, c4XX,L4=1,M=1e12,m=1)
    
    #now, re-introduce the directional dependence dependence
    Dch2_prime = Dch2_radial_prime * np.cos(theta)**2

    #factor 1/2 from expression cancels with integrating from 0-z
    integrand = -Dch2_prime * b_r4 / r_r4
    return integrate.simps(integrand,x=z_r4)
valpha_10_num = np.vectorize(alpha_10_num)

def alpha_10(b_r4,c2XX, c4phi, c4X, c4XX,L4=1,M=1e12,m=1,n_points = 500,z_max=1):
    '''Compute alpha_{10}, relative deflection angle between eigenstate 1 (pure-metric) and EM signals
       given an impact parameter, in units of r4
       b_r4 -> impact parameter in units of r4
       params -> theory parameters
       m -> mass profile
    '''
    
    #c2XX, c4phi, c4X, c4XX = read_params(params)
    params = [c2XX, c4phi, c4X, c4XX]
    #L4 = 1/np.sqrt(abs(c4X))
    L2 = 1 if c2XX == 0 else 1./np.sqrt(abs(c2XX))
    r4 = r4_Mpc(L4=L4,M=M)
    rV = rV_Mpc(L4=L4,M=M,c4phi=c4phi)
    rs = rs_Mpc(M=M)
    
    b_cut = 1e-1*rV/r4
    z_cut = b_cut
    if b_r4 > b_cut:
        z_r4 = np.logspace(min(-5,np.log10(1e-3*z_max),np.log10(1e-3*b_r4)),np.log10(z_max),n_points)
        r_r4 = np.sqrt(z_r4**2 + b_r4**2)
        theta = np.arctan(b_r4/z_r4)
    
        #compute the radial derivative
        Dch2_radial_prime = vdDch2_radial_dr(r_r4,c2XX, c4phi, c4X, c4XX,L4=1,M=1e12,m=1)
    
        #now, re-introduce the directional dependence dependence
        Dch2_prime = Dch2_radial_prime * np.cos(theta)**2

        #factor 1/2 from expression cancels with integrating from 0-z
        integrand = -Dch2_prime * b_r4 / r_r4
        result = integrate.simps(integrand,x=z_r4)
    else:
        if z_max < z_cut:
            result = alpha_10_analytic(b_r4,c2XX, c4phi, c4X, c4XX,L4,M,z_max)
        else:
            integral_analytic = alpha_10_analytic(b_r4,c2XX, c4phi, c4X, c4XX,L4,M,z_cut)
            z_r4 = np.logspace(np.log10(z_cut),np.log10(z_max),n_points)
            r_r4 = np.sqrt(z_r4**2 + b_r4**2)
            theta = np.arctan(b_r4/z_r4)
    
            #compute the radial derivative
            Dch2_radial_prime = vdDch2_radial_dr(r_r4,c2XX, c4phi, c4X, c4XX,L4=1,M=1e12,m=1)
    
            #now, re-introduce the directional dependence dependence
            Dch2_prime = Dch2_radial_prime * np.cos(theta)**2

            #factor 1/2 from expression cancels with integrating from 0-z
            integrand = -Dch2_prime * b_r4 / r_r4
            integral_numeric = integrate.simps(integrand,x=z_r4)
            result = integral_analytic + integral_numeric
    
    return result
valpha_10 = np.vectorize(alpha_10)

def alpha_21(b_r4,c2XX, c4phi, c4X, c4XX,L4=1,M=1e12,m=1,n_points = 500,z_max=1):
    '''Compute alpha_{10}, relative deflection angle between eigenstate 1 (pure-metric) and EM signals
       given an impact parameter, in units of r4
       b_r4 -> impact parameter in units of r4
       params -> theory parameters
       m -> mass profile
    '''
    
    #c2XX, c4phi, c4X, c4XX = read_params(params)
    params = [c2XX, c4phi, c4X, c4XX]
    #L4 = 1/np.sqrt(abs(c4X))
    L2 = 1 if c2XX == 0 else 1./np.sqrt(abs(c2XX))
    r4 = r4_Mpc(L4=L4,M=M)
    rV = rV_Mpc(L4=L4,M=M,c4phi=c4phi)
    rs = rs_Mpc(M=M)
    
    b_cut = 1e-1*rV/r4
    z_cut = b_cut
    if b_r4 > b_cut:
        z_r4 = np.logspace(min(-5,np.log10(1e-3*z_max),np.log10(1e-3*b_r4)),np.log10(z_max),n_points)
        r_r4 = np.sqrt(z_r4**2 + b_r4**2)
        theta = np.arctan(b_r4/z_r4)
    
        #compute the radial derivative
        #1) radial derivative for x/z~0 (theta=0) -> reintroduce direction dependence later
        # -> (\hat k \hat r)^2 = cos(theta)^2 for derivatives
        # -> sin(theta)^2 for Upsilon Can
        radii = np.logspace(np.log10(0.99*min(r_r4)),np.log10(1.01*max(r_r4)),n_points)
        Ups_radial = Upsilon_can(radii,params,rsr4=(rs/r4), theta=np.pi/2.,m=m,across_line=True)
        Dch2_radial = Delta_ch2(radii,params,rsr4=(rs/r4), theta=0,m=m,across_line=True)
        Dcs2_radial = Delta_cs2(radii,params,r2r4=(L4/L2), theta=0,m=m,across_line=True)

        #assume small deviations
        Dc12_2_exp_radial = Ups_radial**2*(Dch2_radial)**2/(Dch2_radial-Dcs2_radial)

        #take the radial derivative
        Dc12_2_exp_radial_prime = np.gradient(Dc12_2_exp_radial,radii)

        #now, interpolate the radial derivative to the radial points and re-introduce the directional dependences (see above)
        Dc12_2_prime = griddata(radii, Dc12_2_exp_radial_prime, r_r4)*np.cos(theta)**2*np.sin(theta)**4

        #factor 1/2 from expression cancels with integrating from 0-z
        integrand = -Dc12_2_prime*np.sin(theta) #*2./2.
        result = integrate.simps(integrand,x=z_r4)
    else:
        if z_max < z_cut:
            result = alpha_21_analytic(b_r4,c2XX, c4phi, c4X, c4XX,L4,M,z_max)
        else:
            integral_analytic = alpha_21_analytic(b_r4,c2XX, c4phi, c4X, c4XX,L4,M,z_cut)
            z_r4 = np.logspace(np.log10(z_cut),np.log10(z_max),n_points)
            r_r4 = np.sqrt(z_r4**2 + b_r4**2)
            theta = np.arctan(b_r4/z_r4)
    
            #compute the radial derivative
            #1) radial derivative for x/z~0 (theta=0) -> reintroduce direction dependence later
            # -> (\hat k \hat r)^2 = cos(theta)^2 for derivatives
            # -> sin(theta)^2 for Upsilon Can
            radii = np.logspace(np.log10(0.99*min(r_r4)),np.log10(1.01*max(r_r4)),n_points)
            Ups_radial = Upsilon_can(radii,params,rsr4=(rs/r4), theta=np.pi/2.,m=m,across_line=True)
            Dch2_radial = Delta_ch2(radii,params,rsr4=(rs/r4), theta=0,m=m,across_line=True)
            Dcs2_radial = Delta_cs2(radii,params,r2r4=(L4/L2), theta=0,m=m,across_line=True)

            #assume small deviations
            Dc12_2_exp_radial = Ups_radial**2*(Dch2_radial)**2/(Dch2_radial-Dcs2_radial)

            #take the radial derivative
            Dc12_2_exp_radial_prime = np.gradient(Dc12_2_exp_radial,radii)

           
            Dc12_2_prime = griddata(radii, Dc12_2_exp_radial_prime, r_r4)*np.cos(theta)**2*np.sin(theta)**4

            #factor 1/2 from expression cancels with integrating from 0-z
            integrand = -Dc12_2_prime*np.sin(theta) #*2./2.
            integral_numeric = integrate.simps(integrand,x=z_r4)
            result = integral_analytic + integral_numeric
    
    return result
valpha_21 = np.vectorize(alpha_21)
