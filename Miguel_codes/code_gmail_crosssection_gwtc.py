import csv
import numpy as np
#import mpmath as mp #problem with mp!
#import matplotlib.pyplot as plt
from scipy.integrate import simps,cumtrapz
import astropy.cosmology as co
import astropy.units as u
import astropy.constants as const

H0kmsMpc = 70*u.km/u.s/u.Mpc
cosmo = co.FlatLambdaCDM(H0kmsMpc,0.307)
def tau_einst_int(z_s):
''' standard lensing optical depth integral (dimensionless)'''
z_p = np.linspace(1e-3*z_s,z_s,100)
DL = cosmo.angular_diameter_distance(z_p)
DLS = cosmo.angular_diameter_distance_z1z2(z_p,z_s*np.ones(len(z_p)))
DS = cosmo.angular_diameter_distance(z_s)
#convert by hand
H = cosmo.H(z_p)/(3e5*u.km/u.s)
H0 = cosmo.H(0)/(3e5*u.km/u.s)
integrand = H0**2*(1+z_p)**2/H*(DL*DLS/DS)
return simps(integrand,x=z_p)
tau_einst_int = np.vectorize(tau_einst_int)
def tau_phys_int(z_s):
'''optical depth integral for fix physical radius (not comoving)'''
z_p = np.linspace(1e-3*z_s,z_s,100)
#DL = cosmo.angular_diameter_distance(z_p)
#DLS = cosmo.angular_diameter_distance_z1z2(z_p,z_s)
#DS = cosmo.angular_diameter_distance(z_s)
#convert by hand
H = cosmo.H(z_p)/(3e5*u.km/u.s)
H0 = cosmo.H(0)/(3e5*u.km/u.s)
integrand = H0*(1+z_p)**2/H
return simps(integrand,x=z_p)
tau_phys_int = np.vectorize(tau_phys_int)
def dL2z(dL,zmax=2):
''' return z for a given d_L, cosmo is an astropy cosmology class'''
zs = np.geomspace(1e-2,zmax,100)
dLs = cosmo.luminosity_distance(zs)

z = griddata(dLs,zs,dL)

return z
# Read data and construct sub-catalogue
reader = csv.DictReader(open('gwtc.csv'))
#print(gw_z_data)
events_tested = ['GW200129_065458', 'GW190412', 'GW200112_155838', 'GW200224_222234', 'GW190521_074359']
subcatalogue = []
subcat_zs = []
print ('name' , 'luminosity_distance', 'z', 'network_matched_filter_snr')
for row in reader:
if row['commonName'] in events_tested:
subcatalogue.append(row)
z = dL2z(cosmo,row['luminosity_distance'])
subcat_zs.append(z)
print (row['commonName'] , row['luminosity_distance'], '%.3g'%z, '%.4g'%float(row['network_matched_filter_snr']))

factor = 3 #factor at which the exclusion can be placed, from poisson statistics CDF = exp(-tau) = 0.05
#subcatalogue and associated fiducials
subcat_zs = np.array(subcat_zs)
tau_phys=tau_phys_int(subcat_zs)
#print ('tau_phys (R_x=22_kpc)', np.sum(tau_phys))
tau_einst=tau_einst_int(subcat_zs)
#print('tau_einst (alpha=1)', np.sum(tau_einst))

Om = 0.3 #Omega_m
h = 0.7

RX_O2_kpc = 22./np.sqrt(np.sum(h*Om*tau_phys)/factor)
alphaX_O2 = 1/np.sqrt(np.sum(1.5*Om*tau_einst)/factor)
print('-----------------------------------------')d
print ('RX > %.3g'%RX_O2_kpc, 'kpc excluded (95%)', ' tau_tot = %.3g'%np.sum(tau_phys))
print ('alphaX > %.3g'%alphaX_O2, ' excluded (95%)', 'tau_tot = %.3g'%np.sum(tau_einst))


if False:
plt.semilogy(subcat_zs,tau_O2_phys,'+',c='orangered',label=r'Phys, $R_x = 22kpc$')
plt.semilogy(subcat_zs,tau_O2_einst,'*',c='darkcyan',label=r'Einst, $\alpha_x = 1$')
plt.plot([],'k--', label=r'LIGO O1+O2')
plt.legend()#, title=r'$R_0=%.3g$y$^{-1}$Gpc$^{-3}$'%R_30_30)
plt.xscale('log')
plt.xlabel(r'$z$')
plt.ylabel(r'tau (single event)')
plt.grid(alpha=0.25)
plt.savefig('out.png')
