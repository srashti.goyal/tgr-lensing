#functions for spherical backgrounds in Horndeski gravity

import numpy as np
import scipy.integrate as integrate
from scipy.interpolate import griddata

#TODO: output is 10^4 but 0.001 instead of 10^{-4}
def latex_float(f):
    float_str = "{0:.2g}".format(f)
    if "e" in float_str:
        base, exponent = float_str.split("e")
        if int(base)==1:
            return r"10^{{{0}}}".format(int(exponent))
        if int(base)==-1:
            return r"-10^{{{0}}}".format(int(exponent))
        else:
            return r"{0} \cdot 10^{{{1}}}".format(base, int(exponent))
    else:
        return float_str
    

#TODO: generalize to seamlessly incorporate more theories
def read_params(param, output_pars = False):
    ''' read theory parameters
        c2 = non-linear term
        TODO -> output parameters
    '''
    global c2, c4phi, c4X, c4XX
    if type(param) == list:
        c2, c4phi, c4X, c4XX = param
        return param
    if param['theory'] == 'G4_mono':
        c2 = param['c2X2']; c4phi = param['c4phi']; 
        nx = param['n4']
        if nx == 1:
            c4X = -1./param['L4']**2 ; c4XX =0;
        elif nx == 2:
            c4XX = 1./param['L4']**2 ; c4X = 0;
        else:
            print('n=%g not coded!'%nx)
            return
        return [c2, c4phi, c4X, c4XX]
    else:
        print('theory='+theory+' not coded!')
        return
    

def scalar_profile(r,param,m=1,nmax=100,precision=1e-6):
    ''' returns d\bar phi/d\tilde r in units of r_4
        r : radial value (can be a vector)
        param : theory parameters
        m : M(r)/M(\infty) fraction of mass contained at radius r, either a number or an array of lenght len(r)
        nmax, precision = max number of iterations and desired precision for Newton's method (precision is relative to x0 parameter)
        NOTE: based on JM's calculation but using Newton's method
    '''

    c2, c4phi, c4X, c4XX = read_params(param)    
     
    
    #polynomial equation for phi
    #x3 phi^3 + x2 phi^2 + x1 phi + x0 = 0
    x3 = (8.)*(c4XX + 2.*c4X**2.) #always a number
    x2 = (4.*r)*(6.*c4X*c4phi)
    x1 = (2.*r**2.)*(1 + 3.*c4phi**2)#changed c2->1 by conventions
    x0 = c4phi*m + np.zeros(len(r)) #make sure it has the right length
    
    phi_vals = np.zeros(len(r))
    
    for i in reversed(range(len(r))):
        #starting point, either approximate solution 
        phi = (m/r)[0] if i==0 else phi_vals[i-1]
        
        #Newton's method, point by point
        for n in range(nmax):
            f = (x3*phi**3 + x2[i]*phi**2 + x1[i]*phi + x0[i])
            if abs(f/max(abs(x0[i]),abs(x1[i]*phi)))<=precision:
                break
            df = (3.*x3*phi**2 + 2*x2[i]*phi + x1[i])          
            phi -= f/df
        #print(n)
        phi_vals[i] = phi
        
    return np.array(phi_vals)


#mass distributions
#https://en.wikipedia.org/wiki/Dark_matter_halo

def m_uniform(radii,r0):
    ''' mass fraction for a uniform sphere of radius r'''
    m = [(r/r0)**3 if r<=r0 else 1. for r in radii]
    return np.array(m)
    
def m_SIS(radii,r_max=0):
    ''' mass fraction for singular-isothermal sphere
        NOTE: 3d mass density, not projected mass as in lensing stuff
        rho ~ sigma_v^2/(2piG r^2)
        M(r) ~ r
        r_core = core radius
        r_max = maximum radius
    '''
    if r_max == 0:
        r_max = max(radii)
    Mtot = r_max
    m = radii/r_max
    for i,r in enumerate(radii):
        if r >= r_max:
            m[i] = 1
    #m *= np.array([1 if r <= r_max else 0 for r in radii])
    return m

def m_PIH(radii,r_core,r_max=0):
    ''' mass fraction for pseudo-isothermal halo
        rho = rho_0/(1+(r/r_c)^2)
        M(r) = 4pi rho_0(r/rc - tan^{-1}(r/rc))
        r_core = core radius
        r_max = maximum radius
    '''
    if r_max == 0:
        r_max = max(radii)
    Mtot = r_max/r_core - np.arctan(r_max/r_core)
    m = (radii/r_core - np.arctan(radii/r_core))/Mtot
    for i,r in enumerate(radii):
        if r >= r_max:
            m[i] = 1
    #m *= np.array([1 if r <= r_max else 0 for r in radii])
    return m


# Scales of the problem

def H0_iMpc(h=0.7):
    '''Hubble parameter in inverse Mpc'''
    return 0.00023349*h/0.7

def rV_Mpc(L4=1,c4phi=1,M=1e12, h=0.7):
    '''quartic Vainshtein radius in units of Mpc
       c4phi: coupling
       L4: scale of the theory [H0]
       M: mass [solar masses]
       h: reduced Hubble param
    '''
    #1km = 3.2408e-20 Mpc
    #2.95 km/Msun
    V_v = c4phi*2.95*M*3.2408e-20/(L4*H0_iMpc(h))**2
    return V_v**(1./3.)

print(rV_Mpc(L4=10,c4phi=0.01,M=1e12))

def r4_Mpc(L4=1,M=1e12, h=0.7):
    '''quartic Vainshtein radius for c_4phi = 1 in units of Mpc
       L4: scale of the theory [H0]
       M: mass [solar masses]
       h: reduced Hubble param
    '''
    #1km = 3.2408e-20 Mpc
    #2.95 km/Msun
    V_v = 2.95*M*3.2408e-20/(L4*H0_iMpc(h))**2
    return V_v**(1./3.)

def rs_Mpc(M=1e12):
    '''Schwarzschild radius in Mpc
       M: mass [solar masses]
    '''
    #1km = 3.2408e-20 Mpc
    #2.95 km/Msun
    r_s = 2.95*M*3.2408e-20
    return r_s


# Cosmological results

def phi_dot0(params,h=0.7, iMpc = False):
    ''' returns phi_dot in units of H0 (or inverse Mpc)
        approximate cosmological solution
        assumes: G4 ~ 0, 
                 canonical field with coupling, 
                 matter domination (eq 41 of 2003.06396)
    '''
    c2, c4phi, c4X, c4XX = read_params(params) 
    
    if c2==0 or c4phi < 1./np.sqrt(c2): #unscreened
        phi_dot0 = c4phi
    else:
        phi_dot0 = (c4phi/c2)**(1./3.) #screened
    
    if iMpc == True:
        phi_dot0 *=H0_iMpc(h)
        
    return phi_dot0


def alpha_T(params,h=0.7):
    ''' returns alpha_T, relies on the time derivative
    '''
    
    phi_dot = phi_dot0(params, h=h,iMpc=False)
    
    c2, c4phi, c4X, c4XX = read_params(params) 
    
    #check signs of c4X!
    tildeX = phi_dot**2/2.
    alpha_T = 4*c4X*(-tildeX) + 8*c4XX*tildeX**2
        
    return alpha_T


def Lambda_4_monomial_from_alpha_T(params, n, aT=1e-15,h=0.7):
    '''returns Lambda_4/H0 so alpha_T equals a specific value
       TODO: some work-around to handle monomial G4s
    '''

    phi_dot = phi_dot0(params, h=h,iMpc=False)
    
    tildeX = phi_dot**2/2.
    #if type(params) == dict
    
    L4 = (aT/(4.*tildeX))**(-1./(2.*n))
    
    #if (c4X == 0 && n==2) or (c4XX == 0 && n==1) #all is fine
    return L4

def L4_from_rV_Mpc(rV=1,c4phi=1,M=1e12, h=0.7):
    '''scale L4/H0 such that rV has a certain value (in Mpc)
       c4phi: coupling
       L4: scale of the theory [H0]
       M: mass [solar masses]
       h: reduced Hubble param
    '''
    #1km = 3.2408e-20 Mpc
    #2.95 km/Msun
    L_4 = np.sqrt(c4phi*2.95*M*3.2408e-20/(rV**3*H0_iMpc(h)**2))
                  
    return L_4


#speeds, mixing, terms in the equations

def Upsilon_can(r_r4, params,rsr4,r2r4=1.,m=1,theta=np.pi/2.,across_line=False):
    '''kinetic mixing coeficient
       r_r4 -> array for radial values (units of r_4)
       params -> theory parameters
       rsr4 -> r_s/r_4
       r2r4 -> Lambda_4/Lambda_2
       m -> mass profile
       theta -> angle with propagation (array or number)
    '''
    
    c2, c4phi, c4X, c4XX = read_params(params) 
    
    dphi_bar = scalar_profile(r_r4,params,m=m)
    ddphi_bar = np.gradient(dphi_bar,r_r4)
    
    #Sum 4.*n*c4Xn*(rSrV**(n-1.))*(dphi_bar**(2.*(n-1)))
    Upsilon = 0.
    for (n,c4Xn) in [(1,c4X),(2,c4XX)]:
        Upsilon += 4.*n*c4Xn*(rsr4**(n-1.))*(dphi_bar**(2.*(n-1)))
    Upsilon *= (ddphi_bar-dphi_bar/r_r4)
    
    Norm = 1
    if c2 !=0:
        Norm = (1+4*c2*dphi_bar**2*r2r4**4)
        print('need to fix r2r4 in Upsilon_can!')
    
    #multiply radial and angular dependences
    radial_dep = Upsilon/Norm
    angular_dep = np.sin(theta)**2 #no angular dependence
    
    if (across_line == True):
        return radial_dep*angular_dep
    else:
        return np.outer(radial_dep,angular_dep)
    
    
#GW speed

def Delta_ch2(r_r4, params,rsr4,m=1,theta=0,across_line=False):
    '''GW field velocity before mixing
       r_r4 -> array for radial values (units of r_4)
       params -> theory parameters
       rsr4 -> r_s/r_4
       r2r4 -> Lambda_4/Lambda_2
       m -> mass profile
       theta -> angle with propagation (array or number)
       across_line -> if r_r4 and theta represent a trajectory
    '''
    
    c2, c4phi, c4X, c4XX = read_params(params) 
    
    dphi_bar = scalar_profile(r_r4,params,m=m)
    #ddphi_bar = np.gradient(dphi_bar,r_r4)
    
    #Sum 4.*n*c4Xn*(rSrV**(n-1.))*(dphi_bar**(2.*(n-1)))
    G4X = 0.
    G4 = 1.
    for (n,c4Xn) in [(1,c4X),(2,c4XX)]:
        G4X += -4.*n*c4Xn*(rsr4**(n-1.))*(dphi_bar**(2.*(n-1)))
        G4 += 2.*c4Xn*(rsr4**n)*(dphi_bar**(2.*n))
    
    #multiply radial and angular dependences dphi_\parallel = dphi cos(th)
    radial_dep = G4X/G4*rsr4*dphi_bar**2
    angular_dep = np.cos(theta)**2
    
    if (across_line == True):
        return radial_dep*angular_dep
    else:
        return np.outer(radial_dep,angular_dep)
    
def Delta_cs2(r_r4, params,r2r4,m=1,theta=0,across_line=False):
    '''scalar velocity before mixing
       r_r4 -> array for radial values (units of r_4)
       params -> theory parameters
       rsr4 -> r_s/r_4
       r2r4 -> Lambda_4/Lambda_2
       m -> mass profile
       theta -> angle with propagation (array or number)
       across_line -> if r_r4 and theta represent a trajectory
    '''
    
    c2, c4phi, c4X, c4XX = read_params(params) 
    
    dphi_bar = scalar_profile(r_r4,params,m=m)
    #ddphi_bar = np.gradient(dphi_bar,r_r4)
    
    #Sum 4.*n*c4Xn*(rSrV**(n-1.))*(dphi_bar**(2.*(n-1)))
    G2XX = 2.*c2*r2r4*dphi_bar**2
    G2X = 1. - c2*r2r4*dphi_bar**2
    
    #multiply radial and angular dependences
    radial_dep = G2XX/G2X
    angular_dep = np.cos(theta)**2
    
    if (across_line == True):
        return radial_dep*angular_dep
    else:
        return np.outer(radial_dep,angular_dep)


#TODO: unify time delay calculations under a generic function?
def Dt_r4_10(b_r4,params,L4=1,M=1e12,m=1,n_points = 500,z_max=1, **m_params):
    '''Compute Dt_{10}, the time delay between eigenstate 1 (pure-metric) and EM signals
       given an impact parameter, in units of r4
       b_r4 -> impact parameter in units of r4
       params -> theory parameters
       m -> mass profile function!
    '''
    
    c2XX, c4phi, c4X, c4XX = read_params(params) 
    #L4 = 1/np.sqrt(abs(c4X))
    L2 = 1 if c2XX == 0 else 1./np.sqrt(abs(c2XX))
    r4 = r4_Mpc(L4=L4,M=M)
    rs = rs_Mpc(M=M)
    
    z_r4 = np.logspace(min(-5,np.log10(1e-3*z_max),np.log10(1e-3*b_r4)),np.log10(z_max),n_points)
    r_r4 = np.sqrt(z_r4**2 + b_r4**2)
    theta = np.arctan(b_r4/z_r4)
    
    if (m!=1):
        m = m(r_r4,**m_params)
    
    #Ups = Upsilon_can(r_r4,params,rsr4=(rs/r4), theta=theta,m=m,across_line=True)
    Dch2 = Delta_ch2(r_r4,params,rsr4=(rs/r4), theta=theta,m=m,across_line=True)
    #Dcs2 = Delta_cs2(r_r4,params,r2r4=(L4/L2), theta=theta,m=m,across_line=True)


    #expansions Delta c_1^2 = c_1^2 - c^2
    #c_1^2 = c_h^2
    Dc10_exp = Dch2
    
    #ci = sqrt(cj^2 + Dij) ~ cj + 1/2*Dij/cj + ...
    #Dt = int_{-inf}^inf dz(1/ci - 1/cj) = 2 int_0^inf dz (1/ci-1/cj) = int_0^inf(-Dij)
    #factor 1/2 from the sqrt cancels with integration from 0-z
    integrand = -Dc10_exp #*2./2.
    return integrate.simps(integrand,x=z_r4)


#TODO: alternate between large/small mixings
def Dt_r4_21(b_r4,params,L4=1,M=1e12,m=1,n_points = 500,z_max=1,**m_params):
    '''Compute Dt_{21}, the time delay between eigenstates 1,2 (pure & mostly-metric)
       given an impact parameter, in units of r4
       b_r4 -> impact parameter in units of r4
       params -> theory parameters
       m -> mass profile
    '''
    
    c2XX, c4phi, c4X, c4XX = read_params(params) 
    #L4 = 1/np.sqrt(abs(c4X))
    L2 = 1 if c2XX == 0 else 1./np.sqrt(abs(c2XX))
    r4 = r4_Mpc(L4=L4,M=M)
    rs = rs_Mpc(M=M)
    
    z_r4 = np.logspace(min(-5,np.log10(1e-3*z_max),np.log10(1e-3*b_r4)),np.log10(z_max),n_points)
    r_r4 = np.sqrt(z_r4**2 + b_r4**2)
    theta = np.arctan(b_r4/z_r4)
    
    if (m!=1):
        m = m(r_r4,**m_params)
    
    Ups = Upsilon_can(r_r4,params,rsr4=(rs/r4), theta=theta,m=m,across_line=True)
    Dch2 = Delta_ch2(r_r4,params,rsr4=(rs/r4), theta=theta,m=m,across_line=True)
    Dcs2 = Delta_cs2(r_r4,params,r2r4=(L4/L2), theta=theta,m=m,across_line=True)
    
    #exact results, don't work well
    #Sigma = (2.+Dch2+Dcs2-2.*Ups**2)/(1-Ups**2)
    #Delta = np.sqrt((Dch2-Dcs2)**2+4.*Ups**2*(Dch2)*(Dcs2))/(1-Ups**2)
    #c22 = (Sigma+Delta)/2.
    #c32 = (Sigma-Delta)/2.

    #expansions Delta c_2^2 = c_2^2 - c_1^2
    #c_1^2 = c_h^2
    Dc22_exp = Ups**2*(Dch2)**2/(Dch2-Dcs2)
    
    #ci = sqrt(cj^2 + Dij) ~ cj + 1/2*Dij/cj + ...
    #Dt = int_{-inf}^inf dz(1/ci - 1/cj) = 2 int_0^inf dz (1/ci-1/cj) = int_0^inf(-Dij)
    #factor 1/2 from the sqrt cancels with integration from 0-z
    integrand = -Dc22_exp #*2./2.
    return integrate.simps(integrand,x=z_r4)

# deflection angle differences
def dDch2_radial_dr(r_r4,params,L4=1,M=1e12,m=1):
    "Valid for linear theory"
    
    c2XX, c4phi, c4X, c4XX = read_params(params) 
    L2 = 1 if c2XX == 0 else 1./np.sqrt(abs(c2XX))
    r4 = r4_Mpc(L4=L4,M=M)
    rs = rs_Mpc(M=M)
    rSr4 = rs / r4
    
    dphi_bar = scalar_profile(r_r4,params,m=m)
    ddphi_bar = np.gradient(dphi_bar,r_r4)
    
    return -8. * c4X * rSr4 * dphi_bar * ddphi_bar

def alpha_10(b_r4,params,L4=1,M=1e12,m=1,n_points = 500,z_max=1, **m_params):
    '''Compute alpha_{10}, relative deflection angle between eigenstate 1 (pure-metric) and EM signals
       given an impact parameter, in units of r4
       b_r4 -> impact parameter in units of r4
       params -> theory parameters
       m -> mass profile
    '''
    
    c2XX, c4phi, c4X, c4XX = read_params(params) 
    L2 = 1 if c2XX == 0 else 1./np.sqrt(abs(c2XX))
    r4 = r4_Mpc(L4=L4,M=M)
    rs = rs_Mpc(M=M)
    
    z_r4 = np.logspace(min(-5,np.log10(1e-3*z_max),np.log10(1e-3*b_r4)),np.log10(z_max),n_points)
    r_r4 = np.sqrt(z_r4**2 + b_r4**2)
    theta = np.arctan(b_r4/z_r4)

    if (m!=1):
        m = m(r_r4,**m_params)
    
    #compute the radial derivative
    Dch2_radial_prime = dDch2_radial_dr(r_r4,params,L4=1,M=1e12,m=1)
    
    #now, re-introduce the directional dependence dependence
    Dch2_prime = Dch2_radial_prime * np.cos(theta)**2

    #factor 1/2 from expression cancels with integrating from 0-z
    integrand = -Dch2_prime * np.sin(theta)
    return integrate.simps(integrand,x=z_r4)

def alpha_21(b_r4,params,L4=1,M=1e12,m=1,n_points = 500,z_max=1, **m_params):
    '''Compute alpha_{21}, relative deflection angle between eigenstate 2 (mostly-metric) and 1 (pure-metric)
       given an impact parameter, in units of r4
       b_r4 -> impact parameter in units of r4
       params -> theory parameters
       m -> mass profile
    '''
    
    c2XX, c4phi, c4X, c4XX = read_params(params) 
    L2 = 1 if c2XX == 0 else 1./np.sqrt(abs(c2XX))
    r4 = r4_Mpc(L4=L4,M=M)
    rs = rs_Mpc(M=M)
    
    z_r4 = np.logspace(min(-5,np.log10(1e-3*z_max),np.log10(1e-3*b_r4)),np.log10(z_max),n_points)
    r_r4 = np.sqrt(z_r4**2 + b_r4**2)
    theta = np.arctan(b_r4/z_r4)
    
    if (m!=1):
        m = m(r_r4,**m_params)  
    
    #compute the radial derivative
    #1) radial derivative for x/z~0 (theta=0) -> reintroduce direction dependence later
    # -> (\hat k \hat r)^2 = cos(theta)^2 for derivatives
    # -> sin(theta)^2 for Upsilon Can
    radii = np.logspace(np.log10(0.99*min(r_r4)),np.log10(1.01*max(r_r4)),n_points)
    Ups_radial = Upsilon_can(radii,params,rsr4=(rs/r4), theta=np.pi/2.,m=m,across_line=True)
    Dch2_radial = Delta_ch2(radii,params,rsr4=(rs/r4), theta=0,m=m,across_line=True)
    Dcs2_radial = Delta_cs2(radii,params,r2r4=(L4/L2), theta=0,m=m,across_line=True)
    #CHECK: is across_line problematic?

    #assume small deviations
    Dc12_2_exp_radial = Ups_radial**2*(Dch2_radial)**2/(Dch2_radial-Dcs2_radial)

    #take the radial derivative
    Dc12_2_exp_radial_prime = np.gradient(Dc12_2_exp_radial,radii)

    #now, interpolate the radial derivative to the radial points and re-introduce the directional dependences (see above)
    Dc12_2_prime = griddata(radii, Dc12_2_exp_radial_prime, r_r4)*np.cos(theta)**2*np.sin(theta)**4

    #factor 1/2 from expression cancels with integrating from 0-z
    integrand = -Dc12_2_prime*np.sin(theta) #*2./2.
    return integrate.simps(integrand,x=z_r4)
