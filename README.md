# Tgr Lensing

Testing GR with gravitational lensing. Please cite the paper if you use the code in this repository: 

[Goyal+2023, "Probing lens-induced gravitational-wave birefringence as a test of general relativity" ](https://journals.aps.org/prd/pdf/10.1103/PhysRevD.108.024052)  

```
@article{PhysRevD.108.024052,
  title = {Probing lens-induced gravitational-wave birefringence as a test of general relativity},
  author = {Goyal, Srashti and Vijaykumar, Aditya and Ezquiaga, Jose Mar\'{\i}a and Zumalac\'arregui, Miguel},
  journal = {Phys. Rev. D},
  volume = {108},
  issue = {2},
  pages = {024052},
  numpages = {19},
  year = {2023},
  month = {Jul},
  publisher = {American Physical Society},
  doi = {10.1103/PhysRevD.108.024052},
  url = {https://link.aps.org/doi/10.1103/PhysRevD.108.024052}
}
```
Based on the theory in the paper: [JM & MZ'2020](https://arxiv.org/abs/2009.12187)



## Installation: 

source igwn-py37 conda environment or any conda installation with bilby installed.

`cd package`

`pip install .` add `--user` if using clusterwide anaconda environment.
